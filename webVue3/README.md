# JMAdmin

#### 介绍
基于vue3+Ant Design开发的Admin管理中台后台，支持两种经典布局模板。  
支持自定义布局设置，配置简单。遵循大道至简的淳朴思想，真正开源、开箱即用，目录简单，开发高效！
欢迎大家支持。

#### 示例图
<img src=https://images.gitee.com/uploads/images/2021/1119/140937_ba0df825_1434193.png width=500 />
<img src=https://images.gitee.com/uploads/images/2021/1119/140947_ea6570c7_1434193.png width=500 />
<img src=https://images.gitee.com/uploads/images/2021/1119/140954_62636cbb_1434193.png width=500 />
<img src=https://images.gitee.com/uploads/images/2021/1119/141005_1a9655c8_1434193.png width=500 />
<img src=https://images.gitee.com/uploads/images/2021/1119/141014_eb6f1c3d_1434193.png width=500 />
<img src=https://images.gitee.com/uploads/images/2021/1119/141028_81f1f433_1434193.png width=500 />
<img src=https://images.gitee.com/uploads/images/2021/1119/141036_c880c57a_1434193.png width=500 />
<img src=https://images.gitee.com/uploads/images/2021/1119/141045_6b09f3c7_1434193.png width=500 />


#### 安装教程

1. 获取代码.  `git clone https://gitee.com/return-com/JMadmin.git`
2. 切换目录.  `cd JMadmin`
3. 安装扩展.  `npm install`
4. 运行调试.  `npm run serve`
5. 运行打包.  `npm run build`

#### 目录说明
![目录说明](https://images.gitee.com/uploads/images/2021/1119/122519_9241854b_1434193.png "目录说明.png")

#### 使用说明
1.支持函数 @/config
```
//import {所用函数或对象名称} from "@/config"
目前支持
app  //app配置文件
http //axios封装的http请求，用法与axios一致
refreshScrollTop  //刷新右边主体内容滚动高度,用于分页切换时，滚动视图高度不为0的问题
deepCopy //深拷贝，解决指针问题
cache //缓存，存入：cache(缓存名称，缓存值，时间s)，获取：cache(缓存名称），清空全部：cache(null）,清空单个:cache(缓存名称,null）
```
2.创建新的页面  
第一步：在src/views内你需要的文件夹以及对应的vue页面  
第二步：注册路由。在src/config/default/route.js创建路由，icon图标为:src/assets/icons/你的svg图标名称，如果存在子路由，则该父路由的模板component:routerTpl,
以上就完成基本的页面创建了，接下来就可以进行你自己的英语开发


#### 特技

1.  支持不同的布局方式，如果你都没看上，也可以新增自己的布局
2.  支持不同的主题
3.  支持在配置文件中修改导航菜单的宽度
4.  菜单无限极分类
5.  基于优秀的vue3+Ant Design框架开发

QQ群：  
<img src=https://images.gitee.com/uploads/images/2021/1119/141457_1510a14d_1434193.png width=200 />

#### 参与贡献
感谢以下大佬或组织[排名不分先后]
1.  [Vue](https://cn.vuejs.org/)
2.  [Ant Design Vue](https://next.antdv.com/docs/vue/introduce-cn/)

