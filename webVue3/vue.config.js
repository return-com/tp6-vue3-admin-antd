
const { resolve } = require('path')
// 导入全局主题颜色theme配置
const app = require(resolve('src/config/default/app.js'))
module.exports = {
    chainWebpack: config => {
        config.module.rules.delete("svg"); //重点:删除默认配置中处理svg,
        config.module
            .rule('svg-sprite-loader')
            .test(/\.svg$/)
            .include
            .add(resolve('src/assets/icons')) //处理svg目录
            .end()
            .use('svg-sprite-loader')
            .loader('svg-sprite-loader')
            .options({
                symbolId: 'icon-[name]'
            })
    },
    css: {
        loaderOptions: {
            less: {
                lessOptions: {
                    // 导入全局主题颜色theme配置
                    modifyVars: app['theme'],
                    javascriptEnabled: true,
                },
            },
        },
    },
    publicPath: app['publicPath'],
    assetsDir: app['assetsDir']
}