import { createRouter, createWebHashHistory } from 'vue-router'
import layoutView from '@/layouts/Index'
import {routeList} from '@/config'
// 自定义布局外基础路由
const defaultRoutes =[
  {
    path: '/user/login',
    name: 'user/login',
    meta: { 
      title: '欢迎登陆', 
      keepAlive: true,
      permission: ['/user/login'] 
    },
    component: () => import('@/views/user/Login.vue')
  },
]
// 404页面
const page404 = { 
  path: '/:pathMatch(.*)*', 
  name: '404', 
  component: () => import('@/exception/404.vue') 
}
// 路由
const routes = [
  {
    path: '/',
    name: '/',
    component: layoutView,
    redirect: '/index',
    children:[
      ...routeList,
      page404,
    ]
  },
  ...defaultRoutes,
  page404,
]

const router = createRouter({
  history: createWebHashHistory(),
  routes,
  scrollBehavior() {
    return {x: 0, y: 0}
  }
})

export default router
