//********** 滚动条插件 ************
// 滚动条插件 - 请不要删除
import PerfectScrollbar from 'perfect-scrollbar';
import "classlist-polyfill";
import "perfect-scrollbar/css/perfect-scrollbar.css";
// 自定义指令
export function directive(Vue){
    //============================ 自定义指令 END============================

    /**
     ****************************
     * title: 测试指令，可删除
     * name: v-test
     ****************************
     */
    Vue.directive("test", {
        // 初次创建dom时候
        mounted(el) {
            console.log(el)
        },
        //更新dom的时候
        updated(el) {
            console.log(el)
        }
    })

    // 更多指令...




    //============================ 自定义指令 END ============================
    /**
     ****************************
     * title: 滚动条指令 - 请不要删除
     * name: v-scrollBar
     ****************************
     */
    /**
     * @description 自动判断该更新PerfectScrollbar还是创建它
     * @param {HTMLElement} el - 必填。dom元素
     */
    const el_scrollBar = (el) => {
        //在元素上加点私货，名字确保不会和已有属性重复即可，暂为_mydiy_ps_scrollBar_
        if (el._mydiy_ps_scrollBar_ instanceof PerfectScrollbar) {
            el._mydiy_ps_scrollBar_.update();
        } else {
            //el上挂一份属性
            el._mydiy_ps_scrollBar_ = new PerfectScrollbar(el, {
                suppressScrollX: true,
            });
        }
    };
    //自定义v-scrollBar
    Vue.directive("scrollBar", {
        //初次创建dom时候
        mounted(el) {
            //判断其样式是否存在position 并且position为"fixed", "absolute"或"relative"
            //如果不符合条件，抛个错误。也可以抛个警告然顺便给其position自动加上"relative"
            //PerfectScrollbar实现原理就是对dom注入两个div，一个是x轴一个是y轴，他们两的position都是absolute。
            //absolute是相对于所有父节点里设置了position属性的最近的一个节点来定位的，为了能够正确定位，要给其设置position属性
            const rules = ["fixed", "absolute", "relative", "sticky"];
            if (!rules.includes(window.getComputedStyle(el, null).position)) {
                console.error(`perfect-scrollbar所在的容器的position属性必须是以下之一：${rules.join("、")}`)
            }
            //el上挂一份属性
            el_scrollBar(el);
        },
        //更新dom的时候
        updated(el) {
            el_scrollBar(el);
        }
    })
}