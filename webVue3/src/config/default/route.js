
import routerTpl from '@/layouts/public/routerTpl'
export const routeList = [
  {
    path: '/index',
    name: 'index',
    meta: {
      title: '首页',
      keepAlive: true,
      icon: 'home',
      permission: ['/index']
    },
    component: () => import('@/views/index/Index.vue')
  },
  {
    path: '/menu',
    name: 'menu',
    component: routerTpl,
    meta: {
      title: '菜单',
      keepAlive: true,
      icon: 'menu',
      permission: ['/menu']
    },
    children: [
      {
        path: '/menu/admin/list',
        name: 'menu/admin/list',
        meta: {
          title: '菜单列表',
          keepAlive: true,
          icon: '',
          permission: ['/menu/admin/list']
        },
        component: () => import('@/views/menu/admin/List')
      },
      {
        path: '/menu/admin/api',
        name: 'menu/admin/api',
        meta: {
          title: '接口列表',
          keepAlive: true,
          icon: '',
          permission: ['/menu/admin/api']
        },
        component: () => import('@/views/menu/admin/Api')
      },
    ]
  },
  {
    path: '/manage',
    name: 'manage',
    component: routerTpl,
    meta: {
      title: '管理',
      keepAlive: true,
      icon: 'manage',
      permission: ['/manage']
    },
    children: [
      {
        path: '/manage/user',
        name: 'manage/user',
        meta: {
          title: '管理列表',
          keepAlive: true,
          icon: '',
          permission: ['/manage/user']
        },
        component: () => import('@/views/manage/User')
      },
      {
        path: '/manage/role',
        name: 'manage/role',
        meta: {
          title: '角色列表',
          keepAlive: true,
          icon: '',
          permission: ['/manage/role']
        },
        component: () => import('@/views/manage/Role')
      },
    ]
  },
  {
    path: '/user',
    name: 'user',
    component: routerTpl,
    meta: {
      title: '用户',
      keepAlive: true,
      icon: 'user',
      permission: ['/user']
    },
    children: [
      {
        path: '/user/list',
        name: 'user/list',
        meta: {
          title: '用户列表',
          keepAlive: true,
          icon: '',
          permission: ['/user/list']
        },
        component: () => import('@/views/user/List')
      },
      {
        path: '/user/realname',
        name: 'user/realname',
        meta: {
          title: '实名认证',
          keepAlive: true,
          icon: '',
          permission: ['/user/realname']
        },
        component: () => import('@/views/user/Realname')
      },
      {
        path: '/user/grade',
        name: 'user/grade',
        meta: {
          title: '会员等级',
          keepAlive: true,
          icon: '',
          permission: ['/user/grade']
        },
        component: () => import('@/views/user/Grade')
      },
    ]
  },

]
