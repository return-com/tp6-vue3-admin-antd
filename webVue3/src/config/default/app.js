const { APP_NAME,FOOTER  } = require('../../../public/config')
module.exports = {
    name:APP_NAME,//系统名称
    // 配置默认主题颜色，配置后重新启/编译动生效
    theme:{
        'primary-color': '#1890ff'
    },
    // 布局配置
    layouts:{
        type:'left',//布局方式:dark,left
        showBreadcrumbs:true,//显示面包屑路由导航
        showDrawer:false,//显示主题设置
        // dark混合布局配置 
        dark:{
            theme:'dark',//主题:light,dark
            leftWidth:200,//左侧宽度(px)
            leftLogoHeight:50,//左侧logo高度(px)
        },
        // left混合布局配置
        left:{
            theme:'dark',//主题:light,dark
            leftLogoLeftHeight:64,//左侧logo高度(px)
            // 注意：左侧宽度 = 左边菜单左宽度 + 左侧菜单右宽度
            leftMenuLeftWidth:120,//左侧菜单左宽度(px)
            leftMenuRightWidth:150,//左侧菜单右宽度(px)
        },
        showFooter:true,//显示底部版权
        footerHeight:70,//底部版权高度(px),显示时有效
        footerContent:FOOTER,//底部版权内容,默认居中
    },
    publicPath: '',//应用的部署地址，如 '/admin/'。如果留空，所有资源将使用相对路径。建议为空！
    assetsDir: 'static',//编译后的资源目录
}