
import { cache, getLeftTopRoute } from "@/config";
import store from "@/store";
import { userMenuAuth } from "@/api/menu/List";
import {
    watch,
    reactive
} from "vue";


// 验证菜单
export function isMenuList(list) {
    const adminMenuAuth = cache('adminMenuAuth')
    let state = reactive({
        adminMenuAuth: adminMenuAuth,
        roles: adminMenuAuth['auth'] ? adminMenuAuth['auth'] : [],
    })
    watch(
        () => store.state.adminMenuAuth,
        (data) => {
            state.adminMenuAuth = data
            state.roles = data.auth
        }
    );
    // 超管时
    if (state.adminMenuAuth.is_super) {
        return list
    }
    const isMenuItem = (roles, path) => {
        let fag = false
        for (let i = 0; i < roles.length; i++) {
            if (roles[i]['path'] === path) {
                fag = true
                break
            }
            
        }
        return fag
    }
    const children = (all) => {
        let data = []
        for (let i = 0; i < all.length; i++) {
            if (isMenuItem(state.roles, all[i]['path'])) {
                if (all[i]['children']) {
                    all[i]['children'] = children(all[i]['children'])
                }
                data.push(all[i])
            }
            
        }
        return data
    }
    return children(list)
}

// 远程获取当前用户菜单权限
export function getMenuAuth() {
    userMenuAuth().then((res) => {
        store.commit('saveRouteList', res.data)
    })
}

//左边菜单列表
export function getLeftMenuList(newRouteList = []) {
    let menuList = cache("adminMenuList")
    if(!menuList){
        menuList = []
    }
    newRouteList.length != 0 ? newRouteList : (newRouteList = menuList);
    return getLeftTopRoute(newRouteList);
}