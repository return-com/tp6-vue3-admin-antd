import axios from 'axios'
import { notice,clearLogin } from "@/config/default/funs";
import { BASE_URL } from "../../../public/config";

// 创建 axios 实例
const request = axios.create({
    // 服务端api地址
    baseURL: BASE_URL ? BASE_URL : '',
    // 请求超时时间，25秒, 如果上传大文件需要更长
    timeout: 25 * 1000,
})
// 接口请求拦截
request.interceptors.request.use(config => {
    return config
})
// 接口响应拦截
request.interceptors.response.use((response) => {
    const result = response.data
    if(typeof(result) == 'string'){
        notice('error', '数据格式错误')
        return Promise.reject(result)
    }
    // result.status [ 200正常 500有错误 401未登录 403没有权限访问 404未找到资源]
    if (result.status === 403 || result.status === 404 || result.status === 500) {
        notice('error', result.message)
        return Promise.reject(result)
    }
    // 未登录
    if (result.status === 401) {
        clearLogin()
        window.location.reload()
        return Promise.reject(result)
    }
    return result
}, (error) => {
    // 网络请求出错
    const errMsg = ((error.response || {}).data || {}).message || '请求出现错误，请稍后再试'
    notice('error', errMsg)
    return Promise.reject(error)
})
export { request as http }