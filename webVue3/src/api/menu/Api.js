import { axios } from "@/config";
const api = {
    list: '/AdminApi/list',
    allTree: '/AdminApi/allTree',
    addApi: '/AdminApi/addApi',
    editApi:'/AdminApi/editApi',
    delApi:'/AdminApi/delApi',
}

/**
 * 获取列表
 * @param {*} data
 */
export function list(data) {
    return axios({
        url: api.list,
        method: 'post',
        data
    })
}
/**
 * 获取API接口树列表-无需权限
 * @param {*} data
 */
 export function allTree(data) {
    return axios({
        url: api.allTree,
        method: 'post',
        data
    })
}
/**
 * 新增API接口
 * @param {*} data
 */
 export function addApi(data) {
    return axios({
        url: api.addApi,
        method: 'post',
        data
    })
}
/**
 * 编辑API接口
 * @param {*} data
 */
 export function editApi(data) {
    return axios({
        url: api.editApi,
        method: 'post',
        data
    })
}
/**
 * 删除API接口
 * @param {*} data
 */
 export function delApi(data) {
    return axios({
        url: api.delApi,
        method: 'post',
        data
    })
}
