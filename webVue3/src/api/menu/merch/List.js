import { axios } from "@/config";
const api = {
    list: '/merch.MerchMenu/list',
    addMenu: '/merch.MerchMenu/addMenu',
    editMenu: '/merch.MerchMenu/editMenu',
    delMenu: '/merch.MerchMenu/delMenu',
    addAuth: '/merch.MerchMenu/addAuth',
    editAuth: '/merch.MerchMenu/editAuth',
    assignAuth:'/merch.MerchMenu/assignAuth',
    userMenuAuth:'/merch.MerchMenu/userMenuAuth',
}

/**
 * 获取列表
 * @param {*} data
 */
export function list(data) {
    return axios({
        url: api.list,
        method: 'post',
        data
    })
}
/**
 * 新增菜单
 * @param {*} data
 */
export function addMenu(data) {
    return axios({
        url: api.addMenu,
        method: 'post',
        data
    })
}
/**
 * 编辑菜单
 * @param {*} data
 */
export function editMenu(data) {
    return axios({
        url: api.editMenu,
        method: 'post',
        data
    })
}
/**
 * 删除菜单
 * @param {*} data
 */
export function delMenu(data) {
    return axios({
        url: api.delMenu,
        method: 'post',
        data
    })
}
/**
 * 新增权限
 * @param {*} data
 */
export function addAuth(data) {
    return axios({
        url: api.addAuth,
        method: 'post',
        data
    })
}
/**
 * 编辑权限
 * @param {*} data
 */
export function editAuth(data) {
    return axios({
        url: api.editAuth,
        method: 'post',
        data
    })
}
/**
 * 分配接口
 * @param {*} data
 */
 export function assignAuth(data) {
    return axios({
        url: api.assignAuth,
        method: 'post',
        data
    })
}
/**
 * 获取当前用户菜单权限
 * @param {*} data
 */
 export function userMenuAuth(data) {
    return axios({
        url: api.userMenuAuth,
        method: 'post',
        data
    })
}