import { axios } from "@/config";
const api = {
    list: '/AdminUser/list',
    add: '/AdminUser/add',
    edit:'/AdminUser/edit',
    del:'/AdminUser/del',
    allTree:'/AdminRole/allTree',
}


/**
 * 获取列表
 * @param {*} data
 */
export function list(data) {
    return axios({
        url: api.list,
        method: 'post',
        data
    })
}
/**
 * 获取列表
 * @param {*} data
 */
 export function allTree(data) {
    return axios({
        url: api.allTree,
        method: 'post',
        data
    })
}
/**
 * 新增
 * @param {*} data
 */
 export function add(data) {
    return axios({
        url: api.add,
        method: 'post',
        data
    })
}
/**
 * 编辑
 * @param {*} data
 */
 export function edit(data) {
    return axios({
        url: api.edit,
        method: 'post',
        data
    })
}
/**
 * 删除
 * @param {*} data
 */
 export function del(data) {
    return axios({
        url: api.del,
        method: 'post',
        data
    })
}
