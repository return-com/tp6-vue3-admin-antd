import { axios } from "@/config";
const api = {
    batchUpload: '/Upload/batchfileupload',
    getList: '/Upload/getList',
    geIdsList: '/Upload/geIdsList',
    fileIdDelete: '/Upload/fileIdDelete',
}

/**
 * 文件批量上传
 * @param {*} data
 */
export function batchUpload(data) {
    return axios({
        url: api.batchUpload,
        method: 'post',
        headers: { 'Content-Type': 'multipart/form-data' },
        data
    })
}
/**
 * 文件列表
 * @param {*} data
 */
export function getList(data) {
    return axios({
        url: api.getList,
        method: 'post',
        data
    })
}
/**
 * 文件列表-指定ID集数据
 * @param {*} data
 */
 export function geIdsList(data) {
    return axios({
        url: api.geIdsList,
        method: 'post',
        data
    })
}
/**
 * 文件删除
 * @param {*} data
 */
export function fileIdDelete(data) {
    return axios({
        url: api.fileIdDelete,
        method: 'post',
        data
    })
}