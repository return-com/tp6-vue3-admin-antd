
import { axios } from "@/config";
const api = {
  login: '/passport/login',
  outLogin: '/passport/logout',
  update: '/passport/update',
}

/**
 * 更新用户信息
 * @param {*} data
 */
export function update(data) {
  return axios({
    url: api.update,
    method: 'post',
    data
  })
}
/**
 * 登录
 * @param {*} data
 */
export function login(data) {
  return axios({
    url: api.login,
    method: 'post',
    data
  })
}

/**
 * 退出登录
 * @param {*} data
 */
export function outLogin() {
  return axios({
    url: api.outLogin,
    method:'post'
  })
}