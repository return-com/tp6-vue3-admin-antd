
import { axios } from "@/config";
const api = {
  list: '/User/list',
  recharge: '/User/recharge',
  gradeList: '/User/gradeList',
  updateGrade: '/User/updateGrade',
  deletes: '/User/delete',
}

/**
 * 列表
 * @param {*} data
 */
export function list(data) {
  return axios({
    url: api.list,
    method: 'post',
    data
  })
}
/**
 * 充值
 * @param {*} data
 */
export function recharge(data) {
  return axios({
    url: api.recharge,
    method: 'post',
    data
  })
}
/**
 * 会员等级列表
 * @param {*} data
 */
export function gradeList(data) {
  return axios({
    url: api.gradeList,
    method: 'post',
    data
  })
}
/**
 * 更新会员等级
 * @param {*} data
 */
export function updateGrade(data) {
  return axios({
    url: api.updateGrade,
    method: 'post',
    data
  })
}
/**
 * 删除
 * @param {*} data
 */
export function deletes(data) {
  return axios({
    url: api.deletes,
    method: 'post',
    data
  })
}