
import { axios } from "@/config";
const api = {
  list: '/UserRealname/list',
  add: '/UserRealname/add',
  edit: '/UserRealname/edit',
  plus: '/UserRealname/plus',
  down: '/UserRealname/down',
  deletes: '/UserRealname/delete',

  regionTree: '/UserRealname/regionTree',
  realnameType: '/UserRealname/realnameType',
}

/**
 * 列表
 * @param {*} data
 */
export function list(data) {
  return axios({
    url: api.list,
    method: 'post',
    data
  })
}
/**
 * 新增
 * @param {*} data
 */
export function add(data) {
  return axios({
    url: api.add,
    method: 'post',
    data
  })
}
/**
 * 编辑
 * @param {*} data
 */
export function edit(data) {
  return axios({
    url: api.edit,
    method: 'post',
    data
  })
}
/**
 * 导入
 * @param {*} data
 */
export function plus(data) {
  return axios({
    url: api.plus,
    method: 'post',
    data
  })
}
/**
 * 下载模板
 * @param {*} data
 */
export function down(data) {
  return axios({
    url: api.down,
    method: 'post',
    data
  })
}
/**
 * 删除
 * @param {*} data
 */
export function deletes(data) {
  return axios({
    url: api.deletes,
    method: 'post',
    data
  })
}
/**
 * 获取地区树-无需权限
 * @param {*} data
 */
 export function regionTree(data) {
  return axios({
    url: api.regionTree,
    method: 'post',
    data
  })
}
/**
 * 获取实名身份类型-无需权限
 * @param {*} data
 */
 export function realnameType(data) {
  return axios({
    url: api.realnameType,
    method: 'post',
    data
  })
}