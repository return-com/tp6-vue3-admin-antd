
import { axios } from "@/config";
const api = {
  list: '/UserGrade/list',
  add: '/UserGrade/add',
  edit: '/UserGrade/edit',
  deletes: '/UserGrade/delete',
}

/**
 * 列表
 * @param {*} data
 */
export function list(data) {
  return axios({
    url: api.list,
    method: 'post',
    data
  })
}
/**
 * 新增
 * @param {*} data
 */
export function add(data) {
  return axios({
    url: api.add,
    method: 'post',
    data
  })
}
/**
 * 编辑
 * @param {*} data
 */
export function edit(data) {
  return axios({
    url: api.edit,
    method: 'post',
    data
  })
}
/**
 * 删除
 * @param {*} data
 */
export function deletes(data) {
  return axios({
    url: api.deletes,
    method: 'post',
    data
  })
}