import { createStore } from 'vuex'
import { cache,routeList,deepCopy } from "@/config";
import { isMenuList } from "@/config/utils/permission";
export default createStore({
  state() {
    return {
      // 当前用户信息
      adminUserInfo:{},
      adminMenuAuth:[],
      adminMenuList:[]
    }
  },
  mutations: {
    /**
     * 更新菜单信息
     * @param {*} state 
     * @param {*} shopsMenuAuth 
     */
    saveRouteList (state,adminMenuAuth) {
      state.adminMenuAuth = adminMenuAuth
      cache("adminMenuAuth",adminMenuAuth)
      const routes = deepCopy(routeList)
      // 返回当前登录用户可用菜单
      const menuList = isMenuList(routes)
      state.adminMenuList = menuList
      cache("adminMenuList",menuList)
    },
    /**
     * 更新登录信息
     * @param {*} state 
     * @param {*} shopsUserInfo 
     */
    saveAdminUserInfo(state,adminUserInfo){
      state.adminUserInfo = adminUserInfo
      cache("adminUserInfo",adminUserInfo)
    }
  },
  actions: {
  },
  modules: {
  }
})