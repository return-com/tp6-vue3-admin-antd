import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import {directive} from '@/config'
import Antd from 'ant-design-vue'
import 'ant-design-vue/dist/antd.less'
import globalComponents from '@/components/globalComponents'
let app = createApp(App)
app.use(router)
app.use(store)
app.use(Antd)
app.use(globalComponents)
app.use(directive)
app.mount('#app')