<Storage :active='[45]' :limit="1" :showDel="true" :showUpload="true" @changeActive="change" @ok="ok" ref="storage"/>
<!-- 调用 -->
let state = reactive({
    storage: undefined,
})
state.storage.show()

<!-- api说明 -->
active 选中的ID集列表，不传为不选

limit 限制选中值，不传为不限制

showDel 显示删除按钮，默认不显示

showUpload 显示上传按钮，默认不显示

change 实时选中监听方法

ok 点击确定方法，返回方法选中ID集和文件集两个参数(limit为1时返回id和文件信息)

<!-- 涉及接口 -->
import { axios } from "@/config";
const api = {
    batchUpload: '/Upload/batchfileupload',
    getList: '/Upload/getList',
    geIdsList: '/Upload/geIdsList',
    fileIdDelete: '/Upload/fileIdDelete',
}

/**
 * 文件批量上传
 * @param {*} data
 */
export function batchUpload(data) {
    return axios({
        url: api.batchUpload,
        method: 'post',
        headers: { 'Content-Type': 'multipart/form-data' },
        data
    })
}
/**
 * 文件列表
 * @param {*} data
 */
export function getList(data) {
    return axios({
        url: api.getList,
        method: 'post',
        data
    })
}
/**
 * 文件列表-指定ID集数据
 * @param {*} data
 */
 export function geIdsList(data) {
    return axios({
        url: api.geIdsList,
        method: 'post',
        data
    })
}
/**
 * 文件删除
 * @param {*} data
 */
export function fileIdDelete(data) {
    return axios({
        url: api.fileIdDelete,
        method: 'post',
        data
    })
}