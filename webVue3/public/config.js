// 网站配置
module.exports = {
    //服务端api请求地址,不含路径
    BASE_URL: "http://localhost:82/index.php/admin/",
    // 网站项目名称
    APP_NAME: "D球村票务",
    //底部版权内容,默认居中
    FOOTER:`<a style="font-size: 12px;color: #5e5e5e;">
    D球村票务系统 ©2021 Created by 上官钧墨 v1.0
    </a>`,
};
