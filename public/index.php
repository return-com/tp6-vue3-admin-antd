<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2019 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// [ 应用入口文件 ]
namespace think;

require __DIR__ . '/../vendor/autoload.php';

// 修改X-Powered-By信息
header('X-Powered-By: https');
// 修改服务器信息
header('server: https');
// 跨域请求
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Authorization-Token,Client-Source,X-Requested-With,Origin,Content-Type,Accept");
header('Access-Control-Allow-Methods: POST,GET');

// 执行HTTP应用并响应
$http = (new App())->http;

$response = $http->run();

$response->send();

$http->end($response);
