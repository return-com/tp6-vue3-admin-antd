<?php
namespace app;

use think\db\exception\DataNotFoundException;
use think\db\exception\ModelNotFoundException;
use think\exception\HttpException;
use think\exception\HttpResponseException;
use think\exception\ValidateException;

use think\Response;
use Throwable;
use think\exception\Handle;
use think\facade\Log;
use app\common\exception\BaseException;
/**
 * 应用异常处理类
 */
class ExceptionHandle extends Handle
{
    // 状态码
    private $status;

    // 错误信息
    private $message;

    // 附加数据
    public $data = [];
    /**
     * 不需要记录信息（日志）的异常类列表
     * @var array
     */
    protected $ignoreReport = [
        HttpException::class,
        HttpResponseException::class,
        ModelNotFoundException::class,
        DataNotFoundException::class,
        ValidateException::class,
    ];

    /**
     * 记录异常信息（包括日志或者其它方式记录）
     *
     * @access public
     * @param  Throwable $exception
     * @return void
     */
    public function report(Throwable $exception): void
    {
        // 使用内置的方式记录异常日志
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @access public
     * @param \think\Request   $request
     * @param Throwable $e
     * @return Response
     */
    public function render($request, Throwable $e): Response
    {
        // 添加自定义异常处理机制
        if ($e instanceof BaseException) {
            $this->status = $e->status;
            $this->message = $e->message;
            $this->data = $e->data;
            return $this->returnJson();
        }
        $this->status = config('app.responseCode.error');
        $this->message = $e->getMessage() ?: '很抱歉，服务器内部错误';
        // 将异常写入日志
        $this->errorLog($e);
        // 如果是debug模式, 输出调试信息
        if (is_debug()) {
            return $this->renderDebug($e);
        }
        return $this->returnJson();

        // 其他错误交给系统处理
        // return parent::render($request, $e);
    }
    /**
     * 返回json格式数据
     * @param array $extend 扩展的数据
     * @return \think\response\Json
     */
    private function returnJson($extend = [])
    {
        $jsonData = ['message' => $this->message, 'status' => $this->status, 'data' => $this->data];
        return json(array_merge($jsonData, $extend));
    }
    /**
     * 返回json格式数据 (debug模式)
     * @param Throwable $e
     * @return \think\response\Json
     */
    private function renderDebug(Throwable $e)
    {
        $debug = [
            'name' => get_class($e),
            'file' => $e->getFile(),
            'line' => $e->getLine(),
            'code' => $this->getCode($e),
            'message' => $this->getMessage($e),
            'trace' => $e->getTrace(),
            'source' => $this->getSourceCode($e),
        ];
        return $this->returnJson(['debug' => $debug]);
    }
    /**
     * 将异常写入日志
     * @param Throwable $e
     */
    private function errorLog(Throwable $e)
    {
        // 生成日志内容
        $data = [
            'file' => $e->getFile(),
            'line' => $e->getLine(),
            'message' => $this->getMessage($e),
            'status' => $this->getCode($e),
        ];
        
        $log = "\r\n"."提示：{$data['message']}";
        $log .= "\r\n"."行数：{$data['line']}";
        $log .= "\r\n"."位置：{$data['file']}";
        $log .= "\r\n"."状态：{$data['status']}";
        $log .= "\r\n" .'关联：'. $e->getTraceAsString()."\r\n";
        // 写入日志文件
        Log::record($log, 'error');
    }
}
