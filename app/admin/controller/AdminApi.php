<?php
namespace app\admin\controller;

use think\annotation\Route;
use app\admin\model\AdminApi as AdminApiModel;
class AdminApi extends BaseController
{
	
	/**
     * [list 接口列表]
     * Author：上官钧墨
     * @return [type] [description]
     */
    public function list()
    {
        $model = new AdminApiModel;
        $list = $model->getList();
        return $this->returnSuccess($list);
    }
    /**
     * [list 接口列表-无需权限]
     * Author：上官钧墨
     * @return [type] [description]
     */
    public function allTree()
    {
        $model = new AdminApiModel;
        $list = $model->getList();
        return $this->returnSuccess($list);
    }
    /**
     * [addApi 添加]
     * Author：上官钧墨
     */
    public function addApi()
    {
        $data = $this->postData('');
        $model = new AdminApiModel;
        $res = $model->addApi($data);
        if (!$res) {
            return $this->returnError($model->getError() ?:'添加失败');
        }
        return $this->returnSuccess('添加成功');
    }
    /**
     * [editApi 修改]
     * Author：上官钧墨
     * @return [type] [description]
     */
    public function editApi()
    {
        $data = $this->postData('');
        $model = new AdminApiModel;
        $detail = $model->detail($data['admin_api_id']);
        if (!$detail) {
            return $this->returnError($model->getError() ?:'修改失败');
        }
        $res = $detail->editApi($data);
        if (!$res) {
            return $this->returnError($detail->getError() ?:'修改失败');
        }
        return $this->returnSuccess('修改成功');
    }
    /**
     * [delMenu 删除菜单]
     * Author：上官钧墨
     * @return [type] [description]
     */
    public function delApi()
    {
        $data = $this->postData('');
        $model = new AdminApiModel;
        $detail = $model->detail($data['admin_api_id']);
        if (!$detail) {
            return $this->returnError($model->getError() ?:'删除失败');
        }
        $res = $detail->delApi();
        if (!$res) {
            return $this->returnError($detail->getError() ?:'删除失败');
        }
        return $this->returnSuccess('删除成功');
    }
    
}