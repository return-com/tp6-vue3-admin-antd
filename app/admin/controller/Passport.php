<?php
namespace app\admin\controller;

use app\admin\service\AdminUser as AdminUserService;
use app\admin\model\AdminUser as AdminUserModel;
use app\admin\model\AdminUserRole as AdminUserRoleModel;
use think\facade\Cache;
class Passport extends BaseController
{

    /**
     * [login 用户登录]
     * Author：上官钧墨
     * @return [type] [description]
     */
    public function login()
    {
        $data = $this->postData('');

        $model = new AdminUserModel;
        $userInfo = $model->where(['user_name' => trim($data['user_name'])])->hidden(['create_time','update_time','password'])->find();

        if (empty($userInfo) || $userInfo['is_delete']) {
            return $this->returnError($model->getError() ?: '登录失败, 账号或密码错误');
        }
        // 验证密码是否正确
        if (!pwd($data['password'], $userInfo['password'])) {
            return $this->returnError($model->getError() ?: '登录失败, 账号或密码错误');
        }
        $userInfo=json_decode(json_encode($userInfo),true);
        if ($userInfo['status']) {
            return $this->returnError($model->getError() ?: '登录失败, 账号不可用');
        }
        // 验证是否分角色组组
        $UserRole = new AdminUserRoleModel();
        $res = $UserRole->detail(['admin_user_id'=>$userInfo['admin_user_id']]);
        if (!$userInfo['is_super'] && !$res) {
            return $this->returnError($UserRole->getError() ?: '登录失败, 未分配角色组');
        }

        $HttpsToken = AdminUserService::login($userInfo);
        return $this->returnSuccess([
            'Authorization-Token' => $HttpsToken,
            'adminUserName' => $userInfo['user_name'],
            'adminUserId' => $userInfo['admin_user_id'],
        ], '登录成功');

    }
    /**
     * [update 修改用户信息]
     * Author：上官钧墨
     * @return [type] [description]
     */
    public function update()
    {
        $data = $this->postData('');
        if (empty($data['user_name'])) {
            return $this->returnError('登录名必填');
        }
        $model = new AdminUserModel;
        $admin = $this->admin;
        $adminUserInfo = $admin['user'];
        $saveData = [
            'update_time'=>time()
        ];
        $newUser = $model->where(['admin_user_id' => $adminUserInfo['admin_user_id']])->find();
        if (!$newUser) {
            return $this->returnError($model->getError() ?: '当前账户不存在，请重新登录');
        }
        if ($newUser['user_name'] != $data['user_name']) {
            $userInfo = $model->where(['user_name' => trim($data['user_name'])])->find();
            if ($userInfo) {
                return $this->returnError($model->getError() ?: '该账户名已存在，请更换名称');
            }
            if (!empty($data['password'])) {
                $saveData['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
            }
            $saveData['user_name'] = $data['user_name'];
        }
        $model->where(['admin_user_id' => $adminUserInfo['admin_user_id']])->update($saveData);
        
        $userInfo = $model->where(['user_name' => trim($data['user_name'])])->find();
        $userInfo=json_decode(json_encode($userInfo),true);
        $HttpsToken = AdminUserService::login($userInfo);
        return $this->returnSuccess([
            'Authorization-Token' => $HttpsToken,
            'adminUserName' => $userInfo['user_name'],
            'adminUserId' => $userInfo['admin_user_id'],
        ],'修改成功');
    }
    /**
     * [logout 退出登录]
     * Author：上官钧墨
     * @return [type] [description]
     */
    public function logout()
    {
        AdminUserService::logout();
        return $this->returnSuccess([], '退出成功');
    }
}
