<?php
namespace app\admin\controller;

use think\annotation\Route;
use app\admin\model\AdminMenu as AdminMenuModel;
class AdminMenu extends BaseController
{

    /**
     * [list 菜单列表]
     * Author：上官钧墨
     * @return [type] [description]
     */
    public function list()
    {
        $model = new AdminMenuModel;
        $list = $model->getList();
        return $this->returnSuccess($list);
    }
    /**
     * [list 菜单列表-无需权限1]
     * Author：上官钧墨
     * @return [type] [description]
     */
    public function allTree()
    {
        $model = new AdminMenuModel;
        $list = $model->getList();
        return $this->returnSuccess($list);
    }
    /**
     * [list 菜单列表-无需权限2]
     * Author：上官钧墨
     * @return [type] [description]
     */
    public function allTrees()
    {
        $model = new AdminMenuModel;
        $list = $model->allTree();
        return $this->returnSuccess($list);
    }
    /**
     * [获取当前用户菜单权限--无需权限3]
     * Author：上官钧墨
     * @return [type] [description]
     */
    public function userMenuAuth()
    {
        $model = new AdminMenuModel;
        $admin = $this->admin;
        $list = $model->userMenuAuth($admin['user']);
        $data = [
            'admin_user_id'=> $admin['user']['admin_user_id'],
            'user_name'=> $admin['user']['user_name'],
            'is_super'=> $admin['user']['is_super'],
            'auth' => $list
        ];
        return $this->returnSuccess($data);
    }

    /**
     * [addMenu 添加菜单]
     * Author：上官钧墨
     */
    public function addMenu()
    {
        $data = $this->postData('');
        $model = new AdminMenuModel;
        $res = $model->addMenu($data);
        if (!$res) {
            return $this->returnError($model->getError() ?:'添加失败');
        }
        return $this->returnSuccess('添加菜单成功');
    }
    /**
     * [editMenu 修改菜单]
     * Author：上官钧墨
     * @return [type] [description]
     */
    public function editMenu()
    {
        $data = $this->postData('');
        $model = new AdminMenuModel;
        $detail = $model->detail($data['admin_menu_id']);
        if (!$detail) {
            return $this->returnError($model->getError() ?:'修改失败');
        }
        $res = $detail->editMenu($data);
        if (!$res) {
            return $this->returnError($detail->getError() ?:'修改失败');
        }
        return $this->returnSuccess('修改菜单成功');
    }
    /**
     * [delMenu 删除菜单]
     * Author：上官钧墨
     * @return [type] [description]
     */
    public function delMenu()
    {
        $data = $this->postData('');
        $model = new AdminMenuModel;
        $detail = $model->detail($data['admin_menu_id']);
        if (!$detail) {
            return $this->returnError($model->getError() ?:'删除失败');
        }
        $res = $detail->delMenu();
        if (!$res) {
            return $this->returnError($detail->getError() ?:'删除失败');
        }
        return $this->returnSuccess('删除成功');
    }
    /**
     * [addAuth 新增权限]
     * Author：上官钧墨
     * @return [type] [description]
     */
    public function addAuth()
    {
        $data = $this->postData('');
        $model = new AdminMenuModel;
        $detail = $model->detail($data['admin_menu_id']);
        if (!$detail) {
            return $this->returnError($model->getError() ?:'新增失败，上级不存在');
        }
        $res = $detail->addMenu([
            'pid'=>$data['admin_menu_id'],
            'path'=>$data['path'],
            'name'=>$data['name'],
            'is_action'=>1,
            'sort'=>$data['sort'],
        ]);
        if (!$res) {
            return $this->returnError($detail->getError() ?:'新增失败，请重试');
        }
        return $this->returnSuccess('新增权限成功');
    }
    /**
     * [editAuth 修改权限]
     * Author：上官钧墨
     * @return [type] [description]
     */
    public function editAuth()
    {
        $data = $this->postData('');
        $model = new AdminMenuModel;
        $detail = $model->detail($data['admin_menu_id']);
        $res = $detail->editMenu($data);
        if (!$res) {
            return $this->returnError($detail->getError() ?:'修改失败，请重试');
        }
        return $this->returnSuccess('修改权限成功');
    }
    /**
     * [assignAuth 分配权限的接口]
     * Author：上官钧墨
     * @return [type] [description]
     */
    public function assignAuth()
    {
        $data = $this->postData('');
        $model = new AdminMenuModel;
        $detail = $model->detail($data['admin_menu_id']);
        if (!$detail) {
            return $this->returnError($model->getError() ?:'分配失败，当前数据不存在');
        }
        $res = $detail->assignAuth($data);
        if (!$res) {
            return $this->returnError($detail->getError() ?:'分配失败，请重试');
        }
        return $this->returnSuccess('分配成功');
    }
}
