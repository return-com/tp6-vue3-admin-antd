<?php
declare (strict_types = 1);
namespace app\admin\controller;

use app\BaseController as Base;
use app\admin\service\auth as AuthService;
use app\admin\service\AdminUser as AdminUserService;
class BaseController extends Base
{
    // 登录信息
    protected $admin;
    // 登录验证白名单
    protected $loginAllowApi = [
        // 登录Url
        'Passport/login',
    ];
    /**
     * 初始化
     */
    public function initialize()
    {
        
        // 验证登录
        $this->checkLogin();
        // 强制验证路由
        $this->checkRoute();
    }
    /**
     * [checkLogin 验证登录状态]
     * Author：上官钧墨
     */
    private function checkLogin()
    {
        // 验证当前请求是否在白名单
        if (in_array($this->getRouteUrl(), $this->loginAllowApi)) {
            return true;
        }
        $this->admin = AdminUserService::getLoginInfo();
        // 验证登录状态
        if (empty($this->admin) || (int)$this->admin['is_login'] !== 1) {
            returnError('登录过期，请重新登录',config('app.responseCode.not_login'));
        }
        return true;
    }
	/**
     * 解析当前路url
     */
    protected function getRouteUrl()
    {
        // 控制器名称
        $controller = $this->request->controller();
        // 方法名称
        $action = $this->request->action();
        // 当前url
        return "{$controller}/$action";
    }

    /**
     * [checkRoute 验证路由]
     * Author：上官钧墨
     * @return [type] [description]
     */
    public function checkRoute()
    {
        if (!AuthService::getInstance()->checkRoute('/' . $this->getRouteUrl())) {
            returnError('很抱歉，没有前操权限',config('app.responseCode.not_permission'));
            // returnError('很抱歉，无权限访问:'.$this->getRouteUrl(),config('app.responseCode.not_permission'));
        }
        return true;
    }

    /**
     * 返回json
     * @param int|null $status 状态码
     * @param string $message
     * @param array $data
     * @return array
     */
    protected function returnJson(int $status = null, string $message = '', $data = [])
    {
        return json(compact('status', 'message', 'data'));
    }

    /**
     * 返回操作成功
     * @param array|string $data
     * @param string $message
     * @return array
     */
    protected function returnSuccess($data = [], string $message = 'success')
    {
        if (is_string($data)) {
            $message = $data;
            $data = [];
        };
        return $this->returnJson(config('app.responseCode.success'), $message, $data);
    }

    /**
     * 返回操作失败
     * @param string $message
     * @param array $data
     * @return array
     */
    protected function returnError(string $message = 'error', $data = [])
    {
        return $this->returnJson(config('app.responseCode.error'), $message, $data);
    }

    /**
     * 获取post数据 (数组)
     * @param $key
     * @return mixed
     */
    protected function postData($key = 'form')
    {
        return $this->request->post(empty($key) ? '' : "{$key}/a");
    }
    
}
