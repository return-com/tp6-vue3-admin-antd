<?php
namespace app\admin\controller;

use think\annotation\Route;
use think\facade\Cache;
use app\admin\model\User as UserModel;
use app\admin\model\UserRealname as UserRealnameModel;
class UserRealname extends BaseController
{
	/**
	 * 列表
	 * @return [type] [description]
	 */
	public function list()
	{
		$data = $this->postData('');
        $model = new UserRealnameModel;
        $list = $model->getList($data);
        return $this->returnSuccess($list);
	}
	/**
	 * 获取地区树
	 * @return [type] [description]
	 */
	public function regionTree()
	{
        $model = new UserRealnameModel;
        $list = $model->regionTree();
        return $this->returnSuccess($list);
	}
	/**
	 * 获取实名类型列表
	 * @return [type] [description]
	 */
	public function realnameType()
	{
        $model = new UserRealnameModel;
        $list = $model->realType();
        return $this->returnSuccess($list);
	}
	// 新增
	public function add()
	{
		$data = $this->postData('');
		$model = new UserRealnameModel;
		$detail = $model->detail(['mobile'=>$data['mobile']]);
		if ($detail) {
			return $this->returnError('新增失败，手机号已存在');
		}

		$default = [
            'type'=>$data['type'],
            'name'=>$data['name'],
            'mobile'=>$data['mobile'],
            'id_number'=>$data['id_number'],
            'sort'=>$data['sort'],
            'duties_name'=>$data['duties_name'],
            'province_id'=>$data['region'][0],
            'city_id'=>$data['region'][1],
            'region_id'=>isset($data['region'][2])?$data['region'][2]:'',
            'detail'=>$data['detail'],
            'create_time'=>time(),
            'update_time'=>time(),
    	];
    	$res = $model->insert($default);
    	if ($res) {
    		return $this->returnSuccess('新增成功');
    	}
    	return $this->returnError('新增失败 -1');
	}
	// 编辑
	public function edit()
	{
		$data = $this->postData('');
		$model = new UserRealnameModel;
		$detail = $model->detail(['id'=>$data['id']]);
		if (!$detail) {
			return $this->returnError('编辑失败，该记录不存在');
		}
		if ($detail['mobile'] != $data['mobile']) {
			$_detail = $model->detail(['mobile'=>$data['mobile']]);
			if ($_detail) {
				return $this->returnError('编辑失败，手机号已存在');
			}
		}

		$default = [
            'type'=>$data['type'],
            'name'=>$data['name'],
            'mobile'=>$data['mobile'],
            'id_number'=>$data['id_number'],
            'sort'=>$data['sort'],
            'duties_name'=>$data['duties_name'],
            'province_id'=>$data['region'][0],
            'city_id'=>$data['region'][1],
            'region_id'=>isset($data['region'][2])?$data['region'][2]:'',
            'detail'=>$data['detail'],
            'update_time'=>time(),
    	];
    	$res = $detail->save($default);
    	if ($res) {
    		return $this->returnSuccess('编辑成功');
    	}
    	return $this->returnError('编辑失败 -1');

	}
	// 导入表格
	public function plus()
	{
		$data = $this->postData('');
		$model = new UserRealnameModel;
		$list = $data['info'];
		$default = [];
		// 验证导入表里是否有重复
		$repeated = getRepeatedValues($list,'mobile');
		if ($repeated['status']){
			return $this->returnError('导入表格内手机号：'.$repeated['values'].'重复');
		}
		foreach ($list as $key => $value) {
			$detail = $model->detail(['mobile'=>$value['mobile']]);
			if ($detail) {
				return $this->returnError('导入失败，手机号'.$value['mobile'].'已存在');die;
			}
			array_push($default,[
	            'type'=>$data['type'],
	            'name'=>$value['name'],
	            'mobile'=>$value['mobile'],
	            'id_number'=>$value['id_number'],
	            'duties_name'=>$value['duties_name'],
	            'sort'=>$data['sort'],
	            'province_id'=>$data['region'][0],
	            'city_id'=>$data['region'][1],
	            'region_id'=>isset($data['region'][2])?$data['region'][2]:'',
	            'detail'=>$data['detail'],
	            'create_time'=>time(),
	            'update_time'=>time(),
	    	]);
		}
		$res = $model->insertAll($default);
    	if ($res) {
    		return $this->returnSuccess('导入成功');
    	}
    	return $this->returnError('导入失败 -1');
	}
	/**
	 * 删除
	 * @return [type] [description]
	 */
	public function delete()
	{
		$data = $this->postData('');
		$model = new UserRealnameModel;
		$detail = $model->detail($data['id']);
		if (!$detail) {
			return $this->returnError('删除失败');
		}
		$res = $detail->delete();
		if ($res) {
			return $this->returnSuccess('删除成功');
		}
		return $this->returnError('删除失败 -1');
	}
}
