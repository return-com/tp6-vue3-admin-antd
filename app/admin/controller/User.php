<?php
namespace app\admin\controller;
use think\facade\Cache;
use app\admin\model\User as UserModel;
use app\admin\model\UserGrade;
class User extends BaseController
{
	/**
	 * 列表
	 * @return [type] [description]
	 */
	public function list()
	{
		$data = $this->postData('');
        $model = new UserModel;
        $list = $model->getList($data);
        return $this->returnSuccess($list);
	}
	/**
	 * 充值
	 * @return [type] [description]
	 */
	public function recharge()
	{
		$data = $this->postData('');
		$model = new UserModel;
		$detail = $model->detail($data['user_id']);
		if (!$detail) {
			return $this->returnError('变更失败');
		}
		$res = $model->recharge($data);
		if (!$res) {
            return $this->returnError($model->getError() ?:'变更失败');
        }
		return $this->returnSuccess('变更成功');
	}
	/**
	 * 会员等级
	 * @return [type] [description]
	 */
	public function gradeList()
	{
		$data = $this->postData('');
		$model = new UserGrade;
		$list = $model->all($data);
        return $this->returnSuccess($list);
	}
	/**
	 * 更新等级
	 * @return [type] [description]
	 */
	public function updateGrade()
	{
		$data = $this->postData('');
		$model = new UserModel;
		$detail = $model->detail($data['user_id']);
		if (!$detail) {
			return $this->returnError('设置失败');
		}
		$res = $model->updateGrade($data,$detail);
		if (!$res) {
            return $this->returnError($model->getError() ?:'设置失败');
        }
		return $this->returnSuccess('设置成功');
	}
	/**
	 * 删除会员
	 * @return [type] [description]
	 */
	public function delete()
	{
		$data = $this->postData('');
		$model = new UserModel;
		$detail = $model->detail($data['user_id']);
		if (!$detail) {
			return $this->returnError('删除失败');
		}
		$res = $detail->save(['is_delete'=>1]);
		if ($res) {
			return $this->returnSuccess('删除成功');
		}
		return $this->returnError('删除失败 -1');
	}
}
