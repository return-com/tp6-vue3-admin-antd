<?php
namespace app\admin\controller;

use think\annotation\Route;
use think\facade\Cache;
use app\admin\model\User as UserModel;
use app\admin\model\UserGrade as UserGradeModel;
class UserGrade extends BaseController
{
	/**
	 * 列表
	 * @return [type] [description]
	 */
	public function list()
	{
		$data = $this->postData('');
        $model = new UserGradeModel;
        $list = $model->getList($data);
        return $this->returnSuccess($list);
	}
	/**
	 * 薪资
	 * @return [type] [description]
	 */
	public function add()
	{
		$data = $this->postData('');
		$model = new UserGradeModel;
		$detail = $model->detail(['level'=>$data['level']]);
		if ($detail) {
			return $this->returnError('该等级权重已存在');
		}
		$res = $model->add($data);
		if (!$res) {
            return $this->returnError($model->getError() ?:'新增失败');
        }
		return $this->returnSuccess('新增成功');
	}
	/**
	 * 编辑
	 * @return [type] [description]
	 */
	public function edit()
	{
		$data = $this->postData('');
		$model = new UserGradeModel;
		$detail = $model->detail($data['grade_id']);
		if (!$detail) {
			return $this->returnError('该等级不存在');
		}
        $res = $detail->edit($data);
		if (!$res) {
            return $this->returnError($model->getError() ?:'编辑失败');
        }
		return $this->returnSuccess('编辑成功');
	}
	/**
	 * 删除会员
	 * @return [type] [description]
	 */
	public function delete()
	{
		$data = $this->postData('');
		$model = new UserGradeModel;
		$user = new UserModel;
		$detail = $model->detail($data['grade_id']);
		if (!$detail) {
			return $this->returnError('删除失败');
		}
		$userRes = $user->detail(['grade_id'=>$data['grade_id']]);
		if ($userRes) {
			return $this->returnError('删除失败,该等级下存在用户');
		}
		$res = $detail->delete();
		if ($res) {
			return $this->returnSuccess('删除成功');
		}
		return $this->returnError('删除失败 -1');
	}
}
