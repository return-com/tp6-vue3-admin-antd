<?php
namespace app\admin\controller;

use think\annotation\Route;
use app\admin\model\AdminUser as AdminUserModel;
use app\admin\model\AdminRoleMenu as AdminRoleMenuModel;
class AdminUser extends BaseController
{
	
	/**
     * [list 列表]
     * Author：上官钧墨
     * @return [type] [description]
     */
    public function list()
    {
        $data = $this->postData('');
        $model = new AdminUserModel;
        $list = $model->getList($data);
        return $this->returnSuccess($list);
    }
    /**
     * [add 添加]
     * Author：上官钧墨
     */
    public function add()
    {
        $data = $this->postData('');
        $model = new AdminUserModel;
        $res = $model->add($data);
        if (!$res) {
            return $this->returnError($model->getError() ?:'添加失败');
        }
        return $this->returnSuccess('添加成功');
    }
    /**
     * [edit 修改]
     * Author：上官钧墨
     * @return [type] [description]
     */
    public function edit()
    {
        $data = $this->postData('');
        $model = new AdminUserModel;
        $detail = $model->detail($data['admin_user_id']);
        if (!$detail) {
            return $this->returnError($model->getError() ?:'修改失败');
        }
        $res = $detail->edit($data,$detail);
        if (!$res) {
            return $this->returnError($detail->getError() ?:'修改失败');
        }
        return $this->returnSuccess('修改成功');
    }
    /**
     * [delMenu 删除]
     * Author：上官钧墨
     * @return [type] [description]
     */
    public function del()
    {
        $data = $this->postData('');
        $model = new AdminUserModel;
        $detail = $model->detail($data['admin_user_id']);
        if (!$detail) {
            return $this->returnError($model->getError() ?:'删除失败');
        }
        $res = $detail->del($data);
        if (!$res) {
            return $this->returnError($detail->getError() ?:'删除失败');
        }
        return $this->returnSuccess('删除成功');
    }
    
}