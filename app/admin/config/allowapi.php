<?php
declare (strict_types = 1);
// 商户后台api白名单配置
// 此处定义的api所有账户均有权访问
// Auth类: app\admin\service\Auth.php
return [
    // 用户登录
    '/Passport/login',
    // 退出登录
    '/Passport/logout',
    // 修改当前用户信息
    '/Passport/update',

    // *******后台部分*******
    // 后台首页
    '/index/index',
    // 获取菜单树列表
    '/AdminMenu/allTree',

    // 获取菜单树列表-带下级权限
    '/AdminMenu/allTrees',
    // 获取当前用户菜单权限
    '/AdminMenu/userMenuAuth',

    // 获取API接口树列表-超管
    '/AdminApi/allTree',

    // 获取角色分组列表
    '/AdminRole/allTree',


];