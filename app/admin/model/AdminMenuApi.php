<?php
declare (strict_types = 1);
namespace app\admin\model;
/**
 * 超管菜单,接口关系列表模型
 */
class AdminMenuApi extends BaseModel
{
	
	// 定义表名
    protected $name = 'admin_menu_api';

    // 定义主键
    protected $pk = 'admin_menu_api_id';

    // 追加字段
    // protected $append = [''];

    /**
     * 获取指定角色的所有菜单id
     * @param array $adminMenuIds 菜单ID集
     * @return array
     */
    public static function getApiIds(array $adminMenuIds)
    {
        return (new self)->where('admin_menu_id', 'in', $adminMenuIds)->column('admin_api_id');
    }
    /**
     * [detail 获取详情]
     * Author：上官钧墨
     * @param  [type] $where [条件]
     */
    public function detail($where=[], $with = [],$hidden = [])
    {
        is_array($where) ? $filter = $where : $filter[$this->pk] = (int)$where;
        return $this->get($filter, $with,$hidden);
    }
}