<?php
declare (strict_types = 1);
namespace app\admin\model;
/**
 * 会员等级模型
 */
class UserGrade extends BaseModel
{
	
	// 定义表名
    protected $name = 'user_grade';

    // 定义主键
    protected $pk = 'grade_id';

    // 追加字段
    // protected $append = [''];

    /**
     * [filter 查询条件过滤器]
     * Author：上官钧墨
     * @return $data [被过滤数组]
     */
    private function filter($data=[])
    {
 		// 设置要查询的字段
    	$default = [
    		$this->pk=>'',
    		'name'=>'',
    		'grade_id'=>'',
    	];
    	// 合并数据，以设置为准
    	$params = $this->setDefaultValue($data,$default);
    	$filter = [];
    	// 更多查询条件......
    	!empty($params[$this->pk]) && $filter[] = [$this->pk, '=', $params[$this->pk]];
        // 用户等级
        isset($params['grade_id']) && $params['grade_id'] > 0 && $filter[] = ['grade_id', '=', (int)$params['grade_id']];
        // 起止时间
        if (!empty($params['create_time'])) {
            $times = betweenTime($params['create_time']);
            $filter[] = ['create_time', '>=', $times['start_time']];
            $filter[] = ['create_time', '<', $times['end_time'] + 86400];
        }
        return $filter;
    }
    /**
     * [getList 获取菜单树]
     * Author：上官钧墨
     * @return [type] [description]
     */
    public function getList($data)
    {
        $hidden = ['update_time'];
        $page = 1;//设置页码
		$listRows = 15;//设置每页条数

		isset($data['page']) && !empty((int)$data['page'])?$page = $data['page']:$page;
		isset($data['listRows']) && !empty((int)$data['listRows'])?$page = $data['listRows']:$page;

		$model = $this->where($this->filter($data));
		$total = $model->count();
        $list = $model->page($page,$listRows)->hidden($hidden)->select();
        return ['page'=>$page,'total'=>$total,'listRows'=>$listRows,'list'=>$list];
    }
    /**
     * 新增
     */
    public function add($data)
    {
        $default = [
            'name'=>$data['name'],
            'level'=>$data['level'],
            'min_expend_money'=>$data['min_expend_money'],
            'status'=>$data['status']?1:0,
            'create_time'=>time(),
            'update_time'=>time(),
        ];
        return $this->insert($default);
    }
    /**
     * 编辑
     */
    public function edit($data)
    {
        $default = [
            'name'=>$data['name'],
            'level'=>$data['level'],
            'min_expend_money'=>$data['min_expend_money'],
            'status'=>$data['status']?1:0,
            'update_time'=>time(),
        ];
        return $this->save($default);
    }
    /**
     * [getList 获取所有]
     * Author：上官钧墨
     * @return [type] [description]
     */
    public function all($data=[])
    {
        $hidden = ['update_time'];
        $model = $this->where($this->filter($data));
        $list = $model->hidden($hidden)->select();
        return $list;
    }
    /**
     * [detail 获取详情]
     * Author：上官钧墨
     * @param  [type] $where [条件]
     */
    public function detail($where=[], $with = [],$hidden = [])
    {
        is_array($where) ? $filter = $where : $filter[$this->pk] = (int)$where;
        return $this->get($filter, $with,$hidden);
    }
}