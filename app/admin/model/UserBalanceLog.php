<?php
declare (strict_types = 1);
namespace app\admin\model;
/**
 * 用余额记录模型
 */
class UserBalanceLog extends BaseModel
{
	
	// 定义表名
    protected $name = 'user_balance_log';

    // 定义主键
    protected $pk = 'id';

    // 追加字段
    // protected $append = [''];

    
    
}