<?php
declare (strict_types = 1);
namespace app\admin\model;
/**
 * 超管角色,菜单关系列表模型
 */
class AdminRoleMenu extends BaseModel
{
	
	// 定义表名
    protected $name = 'admin_role_menu';

    // 定义主键
    protected $pk = 'admin_role_menu_id';

    // 追加字段
    // protected $append = [''];

    /**
     * 获取指定角色的所有菜单id
     * @param array $adminRoleIds 角色ID集
     * @return array
     */
    public static function getMenuIds(array $adminRoleIds)
    {
        return (new self)->where('admin_role_id', 'in', $adminRoleIds)->column('admin_menu_id');
    }
    /**
     * [detail 获取详情]
     * Author：上官钧墨
     * @param  [type] $where [条件]
     */
    public function detail($where=[], $with = [],$hidden = [])
    {
        is_array($where) ? $filter = $where : $filter[$this->pk] = (int)$where;
        return $this->get($filter, $with,$hidden);
    }
}