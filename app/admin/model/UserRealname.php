<?php
declare (strict_types = 1);
namespace app\admin\model;
use app\common\model\Region as RegionModel;
use app\admin\model\UserRealnameType as UserRealnameTypeModel;
/**
 * 会员实名模型
 */
class UserRealname extends BaseModel
{
	
	// 定义表名
    protected $name = 'user_realname';

    // 定义主键
    protected $pk = 'id';

    // 追加字段
    // protected $append = [''];

    /**
     * [filter 查询条件过滤器]
     * Author：上官钧墨
     * @return $data [被过滤数组]
     */
    private function filter($data=[])
    {
 		// 设置要查询的字段
    	$default = [
    		$this->pk=>'',
            'search'=>'',
            'type'=>'',
            'duties_name'=>'',
            'province_id'=>'',
            'city_id'=>'',
            'region_id'=>'',
            'town_id'=>'',
            'detail'=>'',
            'create_time'=>'',
    	];
    	// 合并数据，以设置为准
    	$params = $this->setDefaultValue($data,$default);
    	$filter = [];
    	// 更多查询条件......
    	!empty($params[$this->pk]) && $filter[] = [$this->pk, '=', $params[$this->pk]];
        // 名称/手机号
        !empty($params['search']) && $filter[] = ['name|mobile|id_number', 'like', "%{$params['search']}%"];
        // 起止时间
        if (!empty($params['create_time'])) {
            $times = betweenTime($params['create_time']);
            $filter[] = ['create_time', '>=', $times['start_time']];
            $filter[] = ['create_time', '<', $times['end_time'] + 86400];
        }
        return $filter;
    }
    /**
     * [getList 获取]
     * Author：上官钧墨
     * @return [type] [description]
     */
    public function getList($data)
    {
        $hidden = ['update_time'];
        $page = 1;//设置页码
		$listRows = 10;//设置每页条数

		isset($data['page']) && !empty((int)$data['page'])?$page = $data['page']:$page;
		isset($data['listRows']) && !empty((int)$data['listRows'])?$page = $data['listRows']:$page;

		$model = $this->where($this->filter($data));
		$total = $model->count();

        $addressField = ['id','name'];

        $list = $model->with([
            'user'=>function($query){
                $query->withField(['user_id','mobile','nick_name']);
            },
            'realnameType'=>function($query){
                $query->withField(['id','name']);
            },
            'province'=>function($query)use($addressField){
                $query->withField($addressField);
            },
            'city'=>function($query)use($addressField){
                $query->withField($addressField);
            },
            'region'=>function($query)use($addressField){
                $query->withField($addressField);
            },
        ])->page($page,$listRows)->hidden($hidden)->select();
        return ['page'=>$page,'total'=>$total,'listRows'=>$listRows,'list'=>$list];
    }
    // 获取地区树
    public function regionTree()
    {
        $model = new RegionModel();
        return $model->tree();
    }
    // 获取实名类型列表
    public function realType()
    {
        $model = new UserRealnameTypeModel();
        return $model->all();
    }

    /**
     * 获取用户信息
     */
    public function user()
    {
        return $this->belongsTo("app\admin\model\User", 'mobile','mobile');
    }
    /**
     * 获取用户实名类型
     */
    public function realnameType()
    {
        return $this->belongsTo("app\admin\model\UserRealnameType", 'type','id');
    }
    /**
     * 获取省名
     */
    public function province()
    {
        return $this->belongsTo("app\common\model\Region",'province_id', 'id');
    }
    /**
     * 获取市名
     */
    public function city()
    {
        return $this->belongsTo("app\common\model\Region",'city_id', 'id');
    }
    /**
     * 获取县名
     */
    public function region()
    {
        return $this->belongsTo("app\common\model\Region",'region_id', 'id');
    }
    /**
     * [detail 获取详情]
     * Author：上官钧墨
     * @param  [type] $where [条件]
     */
    public function detail($where=[], $with = [],$hidden = [])
    {
        is_array($where) ? $filter = $where : $filter[$this->pk] = (int)$where;
        return $this->get($filter, $with,$hidden);
    }
}