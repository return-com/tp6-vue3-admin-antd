<?php
declare (strict_types = 1);
namespace app\admin\model;
/**
 * 超管员、角色组关系列表模型
 */
class AdminUserRole extends BaseModel
{
	
	// 定义表名
    protected $name = 'admin_user_role';

    // 定义主键
    protected $pk = 'admin_user_role_id';

    // 追加字段
    // protected $append = [''];
    /**
     * 获取指定管理员的所有角色id
     * @param int $adminUserId
     * @return array
     */
    public static function getRoleIdsByUserId(int $adminUserId)
    {
        return (new static)->where('admin_user_id', '=', $adminUserId)->column('admin_role_id');
    }
    /**
     * [detail 获取详情]
     * Author：上官钧墨
     * @param  [type] $where [条件]
     */
    public function detail($where=[], $with = [],$hidden = [])
    {
        is_array($where) ? $filter = $where : $filter[$this->pk] = (int)$where;
        return $this->get($filter, $with,$hidden);
    }

    
}