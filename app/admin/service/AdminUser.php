<?php
declare (strict_types = 1);
namespace app\admin\service;
use think\facade\Request;
use think\facade\Cache;

/**
 * 超管信息服务
 * Class AdminUser
 * @package app\admin\service
 */
class AdminUser
{
    // 用于生成token的自定义盐
    const TOKEN_SALT = '_admin_user_';
    // 请求管理类
    /* @var $request \think\Request */
    protected $request;

    // 错误信息
    protected $error;

    /**
     * 构造方法
     * BaseService constructor.
     */
    public function __construct()
    {
        // 请求管理类
        $this->request = Request::instance();

    }
    /**
     * 获取错误信息
     * @return mixed
     */
    public function getError()
    {
        return empty($this->error) ? false : $this->error;
    }

    /**
     * 是否存在错误
     * @return bool
     */
    public function hasError()
    {
        return !empty($this->error);
    }

    /**
     * 记录登录信息
     * @param array $userInfo
     * @return bool
     */
    public static function login(array $userInfo)
    {
        // 生成token
        $token = self::makeToken($userInfo['admin_user_id']);
        // 记录缓存, 7天
        Cache::set($token, [
            'user' => $userInfo,
            'is_login' => true,
        ], 86400 * 7);
        return $token;
    }

    /**
     * 获取用户认证Token
     * @return bool|string
     */
    private static function getToken()
    {
        $tokenName = 'Authorization-Token';
        // 获取请求中的token
        $token = request()->header($tokenName);
        if (empty($token)) {
            $token = request()->param($tokenName);
        }
        // 不存在token报错
        if (empty($token)) {
            return false;
        }
        return $token;
    }
    /**
     * 获取当前登录用户的信息
     * @return mixed
     */
    public static function getLoginInfo()
    {
        if (self::getToken() !== false) {
            return Cache::get(self::getToken());
        }
        return false;
    }

    /**
     * 获取当前登录用户的ID
     * @return mixed
     */
    public static function getLoginUserId()
    {
        return static::getLoginInfo()['user']['admin_user_id'];
    }

    /**
     * 清空登录状态
     * @return bool
     */
    public static function logout()
    {
        Cache::delete(self::getToken());
        return true;
    }

    /**
     * 更新登录信息
     * @param array $userInfo
     * @return mixed
     */
    public static function update(array $userInfo)
    {
        return Cache::set(self::getToken(), [
            'user' => $userInfo,
            'is_login' => true,
        ], 86400 * 7);
    }

    /**
     * 生成用户认证的token
     * @param int $admin_user_id
     * @return string
     */
    protected static function makeToken(int $admin_user_id)
    {
        // 生成一个不会重复的随机字符串
        $guid = get_guid_v4();
        // 当前时间戳 (精确到毫秒)
        $timeStamp = microtime(true);
        // 自定义一个盐
        $salt = self::TOKEN_SALT;
        return md5("{$timeStamp}_{$admin_user_id}_{$guid}_{$salt}");
    }

    

}
