<?php
declare (strict_types = 1);
namespace app\mobile\controller;

use app\BaseController as Base;
use think\annotation\Route;
use think\facade\Cache;
use app\mobile\service\User as UserService;
class BaseController extends Base
{
	// 登录信息
    protected $mobile;
	// 登录验证白名单
    protected $loginAllowApi = [
        // 登录Url
        'Passport/login',
        // 短信验证码发送
        'Passport/sendSms',
    ];
    /**
     * 初始化
     */
    public function initialize()
    {
        // 初始化控制器
        $this->initController();
        // 验证登录
        $this->checkLogin();
    }
    /**
     * [初始化控制器]
     * Author：上官钧墨
     * @return [type] [description]
     */
    private function initController()
    {
        
        $arr = ['App','H5','WxApplet'];
        if (!in_array(source(),$arr)) {
            returnError('用户来源类型错误');
        }
        return true;
    }
    /**
     * [checkLogin 验证登录状态]
     * Author：上官钧墨
     */
    private function checkLogin()
    {
        // 验证当前请求是否在白名单
        if (in_array($this->getRouteUrl(), $this->loginAllowApi)) {
            return true;
        }
        $loginInfo = UserService::getLoginInfo();
        // 验证登录状态
        if (empty($loginInfo) || (int)$loginInfo['is_login'] !== 1) {
            returnError('登录过期，请重新登录',config('app.responseCode.not_login'));
        }
        $this->mobile = $loginInfo['user'];
        return true;
    }
    /**
     * 解析当前路url
     */
    protected function getRouteUrl()
    {
        // 控制器名称
        $controller = $this->request->controller();
        // 方法名称
        $action = $this->request->action();
        // 当前url
        return "{$controller}/$action";
    }


    /**
     * 返回json
     * @param int|null $status 状态码
     * @param string $message
     * @param array $data
     * @return array
     */
    protected function returnJson(int $status = null, string $message = '', $data = [])
    {
        return json(compact('status', 'message', 'data'));
    }

    /**
     * 返回操作成功
     * @param array|string $data
     * @param string $message
     * @return array
     */
    protected function returnSuccess($data = [], string $message = 'success')
    {
        if (is_string($data)) {
            $message = $data;
            $data = [];
        };
        return $this->returnJson(config('app.responseCode.success'), $message, $data);
    }

    /**
     * 返回操作失败
     * @param string $message
     * @param array $data
     * @return array
     */
    protected function returnError(string $message = 'error', $data = [])
    {
        return $this->returnJson(config('app.responseCode.error'), $message, $data);
    }

    /**
     * 获取post数据 (数组)
     * @param $key
     * @return mixed
     */
    protected function postData($key = 'form')
    {
        return $this->request->post(empty($key) ? '' : "{$key}/a");
    }


}
