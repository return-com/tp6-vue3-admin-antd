<?php
namespace app\mobile\controller;

use app\mobile\controller\BaseController;
use think\annotation\Route;
use think\facade\Cache;
use app\mobile\service\User as UserService;

class User extends BaseController
{
	/**
	 * [退出登录]
	 * Author：上官钧墨
	 * @param  string $value [description]
	 * @return [type]        [description]
	 */
	public function logout()
	{
		UserService::logout();
		return $this->returnSuccess('退出成功');
	}
	/**
	 * [获取用户信息]
	 * Author：上官钧墨
	 * @return [type] [description]
	 */
	public function userInfo()
	{
		$user = $this->mobile;
		$token = UserService::login($user);
		return $this->returnSuccess([
            'Authorization-Token' => $token,
            'mobile' => $user['mobile'],
            'user_id' => $user['user_id'],
        ], '获取成功');
	}

}
