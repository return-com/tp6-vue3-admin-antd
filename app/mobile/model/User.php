<?php
declare (strict_types = 1);
namespace app\mobile\model;
/**
 * 超管菜单接口列表模型
 */
class User extends BaseModel
{
	
	// 定义表名
    protected $name = 'User';

    // 定义主键
    protected $pk = 'user_id';

    // 追加字段
    // protected $append = [''];

    
}