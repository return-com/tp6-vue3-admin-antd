<?php
namespace app\common\service\sms;

use think\facade\Cache;
use Aliyun\Sms\AliSms;
class AliyunSms
{
	
	/**
	 * [短信验证码发送]
	 * Author：上官钧墨
	 * @param  string $accessKeyId     [您的访问密钥id]
	 * @param  string $accessKeySecret [您的访问密钥密码]
	 * @param  [type] $params          [发送参数]
	 */
	public function sendSms($accessKeyId='',$accessKeySecret='',$params)
	{
		// $accessKeyId = '';// 您的访问密钥id
		// $accessKeySecret = '';// 您的访问密钥密码
		// // 单条发送
		// $params=[
		// 	'PhoneNumbers'=>"17000000000",//短信接收号码
		// 	"SignName"=>"短信签名",//短信签名
		// 	"TemplateCode"=>'SMS_0000001',//短信模板Code
		// 	"TemplateParam"=>[//设置模板参数,模板中存在变量需要替换则为必填项
		// 		"code" => "12345",
		//         "product" => "阿里通信"
		// ];
		$AliSms = new AliSms($accessKeyId,$accessKeySecret);
		return $AliSms->sendSms($params);
	}
}
