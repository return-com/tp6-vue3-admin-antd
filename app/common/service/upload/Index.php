<?php
namespace app\common\service\upload;

use think\facade\Cache;
use app\common\service\upload\storage\Local;
class Index
{
	/**
	 * [单文件上传入口]
	 * Author：上官钧墨
	 * @return [type] [description]
	 */
	public function upload()
	{
		$upload_type = 'Local';//上传类型，本地
		
		if ($upload_type == 'Local') {
			# 上传到本地
			$upload = new Local;
			return $upload->onefileUpload();
		}
		

	}

	/**
	 * [多文件上传入口]
	 * Author：上官钧墨
	 * @return [type] [description]
	 */
	public function batchUpload()
	{
		$upload_type = 'Local';//上传类型，本地
		if ($upload_type == 'Local') {
			# 上传到本地
			$upload = new Local;
			return $upload->batchfileupload();
		}
	}

	/**
	 * [通过ID删除]
	 * Author：上官钧墨
	 * @return [type] [description]
	 */
	public function fileIdDelete($data)
	{
		$upload_type = 'Local';//上传类型，本地
		if ($upload_type == 'Local') {
			# 上传到本地
			$upload = new Local;
			return $upload->fileIdDelete($data['ids']);
		}
	}

}
