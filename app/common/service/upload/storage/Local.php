<?php
namespace app\common\service\upload\storage;
use app\common\model\Storage;
use think\facade\Request;
/**
* TP6文件本地上传类
*/
class Local
{
	/**
	 * [onefileUpload ]单文件上传
	 * @param  [type] $filePath [文件类型名]
	 * @param  [type] $ext      [文件类型验证]
	 * @param  [type] $size     [文件大小验证M]
	 * @return [type]           [description]
	 */
    public function onefileUpload($param=[]){
    	$default = [
	    	'user_id'=>0,
	    	'fileExt'=>['mp3','mp4','jpg','png','gif'],
	    	'fileSize'=>1,
	    ];
    	if (empty($param)) {
    		$param = $default;
    	}else{
    		$param = array_replace($default,$param);
    	}
		$file = request()->file('file');
		if (!is_array($file)) {
			return [
				'status'=>false,
				'message'=>'上传失败,内部错误',
				'data'=>[]
			];die;
		}

		// 上传验证
		$file_ext = strtolower($file->extension());
    	$file_name = md5(uniqid().$file->hash()).'.'.$file_ext;
    	try {
    		// 验证文件类型
	        if (!in_array($file_ext, $default['fileExt'])) {
	        	return [
					'status'=>false,
					'message'=>'上传失败,文件'.$file->getOriginalName().'类型不支持上传',
					'data'=>[]
				];die;
	        }
	        // 文件大小
	        if ($file->getSize()>($param['fileSize']*1024*1024)) {
	        	return [
					'status'=>false,
					'message'=>'上传失败,文件'.$file->getOriginalName().'超出大小限制',
					'data'=>[]
				];die;
	        }

	    } catch (\Exception $e) {
	        return [
				'status'=>false,
				'message'=>'上传失败 -3'.$e,
				'data'=>[]
			];die;
	    }

        // 上传操作
        $filePath = \think\facade\Filesystem::disk('public')->putFileAs(date('Ymd').'/'.$file_ext,$file,$file_name);
        $filePath = str_replace('\\','/',config('filesystem.disks.public.url').'/'.$filePath);
        $savename = [
        	'storage'=>'local',//存储方式
        	'file_type'=>$file->getOriginalMime(),//文件类型
        	'old_file_name'=>$file->getOriginalName(),//原文件名
        	'file_name'=>$file_name,//新文件名
        	'file_path'=>$filePath,//文件路径
        	'file_size'=>$file->getSize(),//文件大小
        	'file_ext'=>$file_ext,//文件扩展名
        	'create_time'=>time(),//文件大小
        	'user_id'=>$param['user_id'],//用户id
        ];
        unset($filePath);
        unset($file_ext);
        unset($file_name);
        
        // 写入记录
        try {
        	$model = new Storage;
	        $res = $model->insert($savename);
	        if ($res) {
	        	return [
					'status'=>true,
					'message'=>'上传成功',
					'data'=>$savename
				];
	        }
	        // 删除上传失败的文件
	        $this->deldir($value['file_path']);
	        return [
				'status'=>false,
				'message'=>'上传失败',
				'data'=>[]
			];
	    } catch (\Exception $e) {
	    	// 删除上传失败的文件
	        $this->deldir($value['file_path']);
	        return [
				'status'=>false,
				'message'=>'上传失败 -1',
				'data'=>[]
			];
	    }

	}
	/**
	 * [batchfileupload 文件批量上传]
	 * @param  [type] $param   [参数]
	 */
    public function batchfileupload($param=[]){
    	$default = [
	    	'user_id'=>0,
	    	'fileExt'=>['mp3','mp4','jpg','png','gif'],
	    	'fileSize'=>1,
	    ];
    	if (empty($param)) {
    		$param = $default;
    	}else{
    		$param = array_replace($default,$param);
    	}
		$files = request()->file('files');
		if (!is_array($files)) {
			return [
				'status'=>false,
				'message'=>'上传失败,内部错误',
				'data'=>[]
			];die;
		}
		// 上传验证
		foreach($files as $file) {
        	$file_ext = strtolower($file->extension());
        	try {
        		// 验证文件类型
		        if (!in_array($file_ext, $default['fileExt'])) {
		        	return [
						'status'=>false,
						'message'=>'上传失败,文件'.$file->getOriginalName().'类型不支持上传',
						'data'=>[]
					];die;
		        }
		        // 文件大小
		        if ($file->getSize()>($param['fileSize']*1024*1024)) {
		        	return [
						'status'=>false,
						'message'=>'上传失败,文件'.$file->getOriginalName().'超出大小限制',
						'data'=>[]
					];die;
		        }

		    } catch (\Exception $e) {
		        return [
					'status'=>false,
					'message'=>'上传失败 -3'.$e,
					'data'=>[]
				];die;
		    }
            unset($file_ext);
        }

        // 上传操作
        $savename = [];
        foreach($files as $file) {
        	$file_ext = strtolower($file->extension());
        	$file_name = md5(uniqid().$file->hash()).'.'.$file_ext;
            $filePath = \think\facade\Filesystem::disk('public')->putFileAs(date('Ymd').'/'.$file_ext,$file,$file_name);
            $filePath = str_replace('\\','/',config('filesystem.disks.public.url').'/'.$filePath);
            $savename[] = [
            	'storage'=>'local',//存储方式
	        	'file_type'=>$file->getOriginalMime(),//文件类型
	        	'old_file_name'=>$file->getOriginalName(),//原文件名
	        	'file_name'=>$file_name,//新文件名
	        	'file_path'=>$filePath,//文件路径
	        	'file_size'=>$file->getSize(),//文件大小
	        	'file_ext'=>$file_ext,//文件扩展名
	        	'create_time'=>time(),//文件大小
	        	'user_id'=>$param['user_id'],//用户id
            ];
            unset($filePath);
            unset($file_ext);
            unset($file_name);
        }
        // 写入记录
        try {
        	$model = new Storage;
	        $res = $model->insertAll($savename);
	        if ($res) {
	        	return [
					'status'=>true,
					'message'=>'上传成功',
					'data'=>$savename
				];
	        }
	        // 删除上传失败的文件
	        foreach ($savename as $key => $value) {
	        	$this->deldir($value['file_path']);
	        }
	        return [
				'status'=>false,
				'message'=>'上传失败',
				'data'=>[]
			];
	    } catch (\Exception $e) {
	    	// 删除上传失败的文件
	        foreach ($savename as $key => $value) {
	        	$this->deldir($value['file_path']);
	        }
	        return [
				'status'=>false,
				'message'=>'上传失败 -1',
				'data'=>[]
			];
	    }
    }
    /**
     * [删除文件]
     * Author：上官钧墨
     * @param  [type] $dir [路径]
     */
    private function deldir($dir){
		$pic1 = app()->getRootPath().'public\\'. $dir;
	    if(file_exists($pic1)){
	        @unlink($pic1);
	        return true;
	    }
	}
	/**
	 * [filedelete 文件删除-通过id]
	 * @return [type] [description]
	 */
	public function fileIdDelete($ids)
	{
		$where = [
			['storage_id','in',$ids],
		];
		$model = (new Storage)->where($where);
		$detail = $model->column('file_path');
		if(!$detail){
			return false;
		}
		$res = $model->delete();
		if(!$res){
			return false;
		}
		foreach ($detail as $key => $val) {
			$this->deldir($val);
		}
	    return true;
	}
	/**
	 * [filedelete 文件删除-通过路径]
	 * @return [type] [description]
	 */
	public function filedelete($dir)
	{
		$pathinfo = pathinfo(str_replace("\\", '/',$dir));
		$model = new Storage;
		$where = array(
			'file_name'=> $pathinfo['basename'],
			'file_ext'=> $pathinfo['extension'],
			'file_path'=>str_replace("\\", '/',$dir)
		);
		$detail = $model->detail($where);
		if(!$detail){
			return false;
		}
		$res = $detail->delete();
		if(!$res){
			return false;
		}
		$this->deldir($dir);
	    return true;
	}
}