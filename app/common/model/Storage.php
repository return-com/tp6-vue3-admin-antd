<?php
declare (strict_types=1);
namespace app\common\model;
use think\Model;
/**
* 文件上传记录模型
*/
class Storage extends Model
{
    // 定义表名
    protected $name = 'storage';

    // 定义主键
    protected $pk = 'storage_id';

    // 追加字段
    // protected $append = [''];
    /**
     * [filter 查询条件过滤器]
     * Author：上官钧墨
     * @return $data [被过滤数组]
     */
    private function filter($data=[])
    {
        // 设置要查询的字段
        $default = [
            $this->pk =>'',
            'storage'=>'',
            'file_type'=>'',
            'search'=>'',
            'file_path'=>'',
            'file_size'=>'',
            'file_ext'=>'',
            'create_time'=>'',
            'user_id'=>'',
            'is_delete'=>0,
        ];
        // 合并数据，以设置为准
        $params = $this->setDefaultValue($data,$default);
        $filter = [];
        // 更多查询条件......
        // 旧名称/新名称
        !empty($params['search']) && $filter[] = ['old_file_name|file_name', 'like', "%{$params['search']}%"];
        // 起止时间
        if (!empty($params['create_time'])) {
            $times = betweenTime($params['create_time']);
            $filter[] = ['create_time', '>=', $times['start_time']];
            $filter[] = ['create_time', '<', $times['end_time'] + 86400];
        }

        return $filter;
    }
    /**
     * [getList 获取]
     * Author：上官钧墨
     * @return [type] [description]
     */
    public function getList($data)
    {
        $hidden = ['update_time','file_size','file_type','is_delete'];
        $page = 1;//设置页码
        $listRows = 18;//设置每页条数

        isset($data['page']) && !empty((int)$data['page'])?$page = $data['page']:$page;
        isset($data['listRows']) && !empty((int)$data['listRows'])?$page = $data['listRows']:$page;
        $model = $this->where($this->filter($data));
        $total = $model->count();

        $list = $model->order('storage_id desc')->page($page,$listRows)->hidden($hidden)->select();
        
        return ['page'=>$page,'total'=>$total,'listRows'=>$listRows,'list'=>$this->setDomain($list)];
    }
    // 设置url
    private function setDomain($list){
        foreach ($list as $key => $val) {
            if ($val['storage'] == 'local') {
                $list[$key]['domain'] = base_url();
            }
        }
        return $list;
    }
    /**
     * [getList 获取指定ID集数据]
     * Author：上官钧墨
     * @return [type] [description]
     */
    public function geIdsList($ids)
    {
        $hidden = ['update_time','file_size','file_type','is_delete'];
        $model = $this->where([
            [$this->pk,'in',$ids],
        ]);
        $list = $model->hidden($hidden)->select();
        return ['list'=>$this->setDomain($list)];
    }
    /**
     * [detail 获取详情]
     * Author：上官钧墨
     * @param  [type] $where [条件]
     */
    public function detail($where=[], $with = [],$hidden = [])
    {
        is_array($where) ? $filter = $where : $filter[$this->pk] = (int)$where;
        return $this->get($filter, $with,$hidden);
    }
    
    /**
     * 设置默认的检索数据
     * @param array $param
     * @param array $default
     */
    protected function setDefaultValue(array $param, array $default = [])
    {
        $res = array_merge($default, $param);
        foreach ($param as $field => $val) {
            // 不存在默认值跳出循环
            if (!isset($default[$field])) continue;
            // 如果传参为空, 设置默认值
            if (empty($val) && $val !== '0') {
                $res[$field] = $default[$field];
            }
        }
        return $res;
    }
    /**
     * 查找单条记录
     * @param $data
     * @param array $with
     */
    public function get($data, $with = [],$hidden = [])
    {
        try {
            $query = $this->with($with);
            return is_array($data) ? $query->where($data)->hidden($hidden)->find() : $query->hidden($hidden)->find((int)$data);
        } catch (\Exception $e) {
            return false;
        }
    }
}