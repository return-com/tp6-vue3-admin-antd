<?php
declare (strict_types = 1);
namespace app\common\exception;

use think\Exception;

/**
 * 自定义异常类的基类
 */
class BaseException extends Exception
{
    // 状态码
    public $status;

    // 错误信息
    public $message = '很抱歉，服务器内部错误';

    // 附加数据
    public $data = [];

    /**
     * 构造函数，接收一个关联数组
     * @param array $params 关联数组只应包含status、message、data，且不应该是空值
     */
    public function __construct($params = [])
    {
        $this->status = array_key_exists('status', $params) ? $params['status']
            : config('app.responseCode.error');

        if (array_key_exists('message', $params)) {
            $this->message = $params['message'];
        }
        if (array_key_exists('data', $params)) {
            $this->data = $params['data'];
        }
    }
}

