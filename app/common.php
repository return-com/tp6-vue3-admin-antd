<?php
// 应用公共文件
use app\common\exception\BaseException;
use think\facade\Env;
use think\facade\Request;
use think\facade\Config;

/**
 * 获取当前域名及根路径
 * @return string
 */
function base_url()
{
    static $baseUrl = '';
    if (empty($baseUrl)) {
        $request = Request::instance();

        $scheme= $request->scheme();
        if(isset($_SERVER['HTTPS'])&&$_SERVER['HTTPS']=='on'){
            $scheme='https';
        }
        // url子目录
        $rootUrl = root_url();
        // 拼接完整url
        $baseUrl = "{$scheme}://" . $request->host() . $rootUrl;
    }
    return $baseUrl;
}
/**
 * 获取当前url的子目录路径
 * @return string
 */
function root_url()
{
    static $rootUrl = '';
    if (empty($rootUrl)) {
        $request = Request::instance();
        $subUrl = str_replace('\\', '/', dirname($request->baseFile()));
        $rootUrl = $subUrl . ($subUrl === '/' ? '' : '/');
    }
    return $rootUrl;
}
/**
 * 获取二维数组中指定Key的重复Value
 * @param array $arrInput 二维数组
 * @param string $strKey 键名
 * @return bool|string 重复的键值（以逗号分隔）
 */
function getRepeatedValues($arrInput, $strKey)
{
    $new = [
        "status"=>false,
        'values'=>false
    ];
    //参数校验
    if (!is_array($arrInput) || empty($arrInput) || empty($strKey)) {
        return false;
    }

    //获取数组中所有指定Key的值，如果为空则表示键不存在
    $arrValues = array_column($arrInput, $strKey);
    if (empty($arrValues)) {
        return false;
    }

    $arrUniqueValues = array_unique($arrValues);
    if (count($arrValues) != count($arrUniqueValues)) {
        $new['status'] = true;
    }
    
    $arrRepeatedValues = array_unique(array_diff_assoc($arrValues, $arrUniqueValues));
    $new['values'] = implode(',',$arrRepeatedValues);
    return $new;
}
/**
 * 格式化起止时间
 * 兼容前端RangePicker组件
 * 2021-12-09T01:55:00.394Z => 1585670400
 * @param array $times
 * @return array
 */
function betweenTime(array $times)
{
    foreach ($times as &$time) {
        $time = trim($time, '&quot;');
        $time = strtotime(date('Y-m-d', strtotime($time)));
    }
    return ['start_time' => current($times), 'end_time' => next($times)];
}
/**
 * [isEmail 验证邮箱]
 * @return [type] [description]
 */
function isEmail($email){
  $preg_email='/^[a-zA-Z0-9]+([-_.][a-zA-Z0-9]+)*@([a-zA-Z0-9]+[-.])+([a-z]{2,5})$/ims';
  if(preg_match($preg_email,$email)){
      return true;
  }else{
      return false;
  }
}
/**
 * [isMobile 验证手机号]
 * @return [type] [description]
 */
function isMobile($phone){
    $preg_phone='/^1[123456789]\d{9}$/ims';
    if(preg_match($preg_phone,$phone)){
        return true;
    }else{
        return false;
    }
}
/**
 * [isMobile 验证手机短信验证码4位]
 * @return [type] [description]
 */
function isCode($code,$length=4){
    $preg_phone='/^[0-9]{'.$length.'}$/ims';
    if(preg_match($preg_phone,$code)){
        return true;
    }else{
        return false;
    }
}
/**
 * 隐藏手机号中间四位 13012345678 -> 130****5678
 * @param string $mobile 手机号
 * @return string
 */
function hide_mobile(string $mobile)
{
    return substr_replace($mobile, '****', 3, 4);
}
// 过滤掉emoji表情
function filterEmoji($str) {
    $str = preg_replace_callback("/./u",
        function (array $match) {
            return strlen($match[0]) >= 4 ? '' : $match[0];
        },
        $str);
    return $str;
}
/**
 * 重写htmlspecialchars方法 (解决int类型过滤后后变为string类型)
 * @param $string
 * @return string
 */
function my_htmlspecialchars($string)
{
    return is_string($string) ? htmlspecialchars($string) : $string;
}
/**
 * [短信验证码]
 * Author：上官钧墨
 * @param  integer $length [description]
 * @return [type]          [description]
 */
function generate_code($length = 4) {
    return rand(pow(10,($length-1)), pow(10,$length)-1);
}
/**
 * [获取平台用户来源]
 * Author：上官钧墨
 */
function source()
{
    $source = request()->header('Client-Source');
    if (empty($source)) {
        $source = request()->param('Client-Source');
    }
    return $source;
}

/**
 * [pwd 密码加密/验证]
 * Author：上官钧墨
 * @param  [type] $password [用户输入]
 * @param  [type] $key      [加密后的密钥]
 */
function pwd($password,$key=''){
    if ($password!='' && $key=='') {
        return password_hash($password, PASSWORD_DEFAULT);
    }
    return password_verify($password,$key);
}
/**
 * 当前是否为调试模式
 * @return bool
 */
function is_debug()
{
    return (bool)Env::instance()->get('APP_DEBUG');
}
/**
 * 输出错误信息
 * @param string $message 报错信息
 * @param int|null $status 状态码
 * @param array $data 附加数据
 */
function returnError(string $message,$status = null, array $data = [])
{
    is_null($status) && $status = config('app.responseCode.error');
    throw new BaseException(['status' => $status, 'message' => $message, 'data' => $data]);
}
/**
 * 获取全局唯一标识符
 * @param bool $trim
 * @return string
 */
function get_guid_v4($trim = true)
{
    // Windows
    if (function_exists('com_create_guid') === true) {
        $charid = com_create_guid();
        return $trim == true ? trim($charid, '{}') : $charid;
    }
    // OSX/Linux
    if (function_exists('openssl_random_pseudo_bytes') === true) {
        $data = openssl_random_pseudo_bytes(16);
        $data[6] = chr(ord($data[6]) & 0x0f | 0x40);    // set version to 0100
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80);    // set bits 6-7 to 10
        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }
    // Fallback (PHP 4.2+)
    mt_srand((double)microtime() * 10000);
    $charid = strtolower(md5(uniqid(rand(), true)));
    $hyphen = chr(45);                  // "-"
    $lbrace = $trim ? "" : chr(123);    // "{"
    $rbrace = $trim ? "" : chr(125);    // "}"
    $guidv4 = $lbrace .
        substr($charid, 0, 8) . $hyphen .
        substr($charid, 8, 4) . $hyphen .
        substr($charid, 12, 4) . $hyphen .
        substr($charid, 16, 4) . $hyphen .
        substr($charid, 20, 12) .
        $rbrace;
    return $guidv4;
}
/**
 * 下划线转驼峰
 * @param $uncamelized_words
 * @param string $separator
 * @return string
 */
function camelize($uncamelized_words, $separator = '_')
{
    $uncamelized_words = $separator . str_replace($separator, " ", strtolower($uncamelized_words));
    return ltrim(str_replace(" ", "", ucwords($uncamelized_words)), $separator);
}

/**
 * 驼峰转下划线
 * @param $camelCaps
 * @param string $separator
 * @return string
 */
function uncamelize($camelCaps, $separator = '_')
{
    return strtolower(preg_replace('/([a-z])([A-Z])/', "$1" . $separator . "$2", $camelCaps));
}
/**
 * [bcsub 数字精度计算 相减]
 */
function my_bcsub($leftOperand, $rightOperand, $scale = 2)
{
    // bcsub(被减数,减数,保留几位小数)
    return \bcsub($leftOperand, $rightOperand, $scale);
}
/**
 * [bcsub 数字精度计算 相加]
 */
function my_bcadd($leftOperand, $rightOperand, $scale = 2)
{
    // bcadd(被加数,加数,保留几位小数);
    // bcadd(1,3,2);//4.00
    return \bcadd($leftOperand, $rightOperand, $scale);
}
/**
 * [bcsub 数字精度计算 相乘]
 */
function my_bcmul($leftOperand, $rightOperand, $scale = 2)
{
    // bcmul(被乘数,乘数,保留几位小数)
    return \bcmul($leftOperand, $rightOperand, $scale);
}
/**
 * [bcsub 数字精度计算 相除]
 */
function my_bcdiv($leftOperand, $rightOperand, $scale = 2)
{
    // bcdiv(被除数,除数,保留几位小数)
    return \bcdiv($leftOperand, $rightOperand, $scale);
}
/**
 * [bcsub 数字精度计算 比较大小]
 *【0表示 相同】 【1 表示 num1大】 【-1 表示 num2 大 或 其他】
 */
function my_bccomp($leftOperand, $rightOperand, $scale = 2)
{
    // bccomp(值1,值2,比较的小数位数)
    return \bccomp($leftOperand, $rightOperand, $scale);
}