<?php

namespace Aliyun\Sms;
/**
 * AliSms 阿里云短信
 */
class AliSms {

	// 您的访问密钥id
	private $accessKeyId;
	// 您的访问密钥密码
	private $accessKeySecret;

    /**
     * [__construct 构造方法]
     * Author：上官均墨
     * @param [type] $accessKeyId     [your access key id]
     * @param [type] $accessKeySecret [your access key secret]
     */
    public function __construct($accessKeyId, $accessKeySecret)
    {
        // parent::__construct();
        $this->accessKeyId = $accessKeyId;
        $this->accessKeySecret = $accessKeySecret;
    }
	/**
	 * [sendSms 单条发送]
		$params=[
		'PhoneNumbers'=>"17000000000",//短信接收号码
		"SignName"=>"短信签名",//短信签名
		"TemplateCode"=>'SMS_0000001',//短信模板Code
		"TemplateParam"=>[//设置模板参数,模板中存在变量需要替换则为必填项
			"code" => "12345",
	        "product" => "阿里通信"
		]]
	 */
	public function sendSms($params)
	{
		header("Content-Type: text/plain; charset=utf-8");

	    // fixme 必填：是否启用https
	    $security = false;
	    // fixme 必填: 请参阅 https://ak-console.aliyun.com/ 取得您的AK信息
	    $accessKeyId = $this->accessKeyId;
	    $accessKeySecret = $this->accessKeySecret;

	    // fixme 可选: 设置发送短信流水号
	    // $params['OutId'] = "12345";

	    // fixme 可选: 上行短信扩展码, 扩展码字段控制在7位或以下，无特殊需求用户请忽略此字段
	    // $params['SmsUpExtendCode'] = "1234567";


	    // *** 以下代码若无必要无需更改 ***
	    if(!empty($params["TemplateParam"]) && is_array($params["TemplateParam"])) {
	        $params["TemplateParam"] = json_encode($params["TemplateParam"], JSON_UNESCAPED_UNICODE);
	    }


	    // 此处可能会抛出异常，注意catch
	    $content = $this->request(
	        $accessKeyId,
	        $accessKeySecret,
	        "dysmsapi.aliyuncs.com",
	        array_merge($params, array(
	            "RegionId" => "cn-hangzhou",
	            "Action" => "SendSms",
	            "Version" => "2017-05-25",
	        )),
	        $security
	    );

	    return $content;
	}

	/**
	 * 批量发送短信
		$params = [
			//短信接收号码,上限为100个
			'PhoneNumberJson'=>[
				"1500000000",
		        "1500000001",
			],
			//短信签名
			"SignNameJson"=>[
				"云通信",
		        "云通信2",
			],
			"TemplateCode"=>'SMS_0000001',//短信模板Code
			"TemplateParamJson"=>[//设置模板参数,模板中存在变量需要替换则为必填项
				[
					"name" => "Tom",
		            "code" => "123",
				],
		        [
					"name" => "Jack",
		            "code" => "456",
				]
		]]
	 */
	public function sendBatchSms($params) {
	    // *** 需用户填写部分 ***
	    // fixme 必填：是否启用https
	    $security = false;

	    // fixme 必填: 请参阅 https://ak-console.aliyun.com/ 取得您的AK信息
	    $accessKeyId = $this->accessKeyId;
	    $accessKeySecret = $this->accessKeySecret;


	    // todo 可选: 上行短信扩展码, 扩展码字段控制在7位或以下，无特殊需求用户请忽略此字段
	    // $params["SmsUpExtendCodeJson"] = json_encode(array("90997","90998"));


	    // *** 以下代码若无必要无需更改 ***
	    $params["TemplateParamJson"]  = json_encode($params["TemplateParamJson"], JSON_UNESCAPED_UNICODE);
	    $params["SignNameJson"] = json_encode($params["SignNameJson"], JSON_UNESCAPED_UNICODE);
	    $params["PhoneNumberJson"] = json_encode($params["PhoneNumberJson"], JSON_UNESCAPED_UNICODE);

	    if(!empty($params["SmsUpExtendCodeJson"]) && is_array($params["SmsUpExtendCodeJson"])) {
	        $params["SmsUpExtendCodeJson"] = json_encode($params["SmsUpExtendCodeJson"], JSON_UNESCAPED_UNICODE);
	    }


	    // 此处可能会抛出异常，注意catch
	    $content = $this->request(
	        $accessKeyId,
	        $accessKeySecret,
	        "dysmsapi.aliyuncs.com",
	        array_merge($params, array(
	            "RegionId" => "cn-hangzhou",
	            "Action" => "SendBatchSms",
	            "Version" => "2017-05-25",
	        )),
	        $security
	    );

	    return $content;
	}

	/**
	 * 短信发送记录查询
		$params=[
		'PhoneNumber'=>"17000000000",//短信接收号码
		"SendDate"=>"短信签名",//短信发送日期，格式Ymd，支持近30天记录查询
		"PageSize"=>10,//分页大小
		"CurrentPage"=>1,//当前页码
	 */
	public function querySendDetails($params) {

	    $params = array ();

	    // *** 需用户填写部分 ***
	    // fixme 必填：是否启用https
	    $security = false;

	    // fixme 必填: 请参阅 https://ak-console.aliyun.com/ 取得您的AK信息
	    $accessKeyId = $this->accessKeyId;
	    $accessKeySecret = $this->accessKeySecret;

	    // *** 以下代码若无必要无需更改 ***

	    // 此处可能会抛出异常，注意catch
	    $content = $this->request(
	        $accessKeyId,
	        $accessKeySecret,
	        "dysmsapi.aliyuncs.com",
	        array_merge($params, array(
	            "RegionId" => "cn-hangzhou",
	            "Action" => "QuerySendDetails",
	            "Version" => "2017-05-25",
	        )),
	        $security
	    );

	    return $content;
	}

	// ******************** 华丽分割线 ********************
    /**
     * 生成签名并发起请求
     *
     * @param $accessKeyId string AccessKeyId (https://ak-console.aliyun.com/)
     * @param $accessKeySecret string AccessKeySecret
     * @param $domain string API接口所在域名
     * @param $params array API具体参数
     * @param $security boolean 使用https
     * @param $method boolean 使用GET或POST方法请求，VPC仅支持POST
     * @return bool|\stdClass 返回API接口调用结果，当发生错误时返回false
     */
    private function request($accessKeyId, $accessKeySecret, $domain, $params, $security=false, $method='POST') {
        $apiParams = array_merge(array (
            "SignatureMethod" => "HMAC-SHA1",
            "SignatureNonce" => uniqid(mt_rand(0,0xffff), true),
            "SignatureVersion" => "1.0",
            "AccessKeyId" => $accessKeyId,
            "Timestamp" => gmdate("Y-m-d\TH:i:s\Z"),
            "Format" => "JSON",
        ), $params);
        ksort($apiParams);

        $sortedQueryStringTmp = "";
        foreach ($apiParams as $key => $value) {
            $sortedQueryStringTmp .= "&" . $this->encode($key) . "=" . $this->encode($value);
        }

        $stringToSign = "${method}&%2F&" . $this->encode(substr($sortedQueryStringTmp, 1));

        $sign = base64_encode(hash_hmac("sha1", $stringToSign, $accessKeySecret . "&",true));

        $signature = $this->encode($sign);

        $url = ($security ? 'https' : 'http')."://{$domain}/";

        try {
            $content = $this->fetchContent($url, $method, "Signature={$signature}{$sortedQueryStringTmp}");
            return json_decode($content,true);
        } catch( \Exception $e) {
            return false;
        }
    }

    private function encode($str)
    {
        $res = urlencode($str);
        $res = preg_replace("/\+/", "%20", $res);
        $res = preg_replace("/\*/", "%2A", $res);
        $res = preg_replace("/%7E/", "~", $res);
        return $res;
    }

    private function fetchContent($url, $method, $body) {
        $ch = curl_init();

        if($method == 'POST') {
            curl_setopt($ch, CURLOPT_POST, 1);//post提交方式
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        } else {
            $url .= '?'.$body;
        }

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "x-sdk-client" => "php/2.0.0"
        ));

        if(substr($url, 0,5) == 'https') {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        }

        $rtn = curl_exec($ch);

        if($rtn === false) {
            // 大多由设置等原因引起，一般无法保障后续逻辑正常执行，
            // 所以这里触发的是E_USER_ERROR，会终止脚本执行，无法被try...catch捕获，需要用户排查环境、网络等故障
            trigger_error("[CURL_" . curl_errno($ch) . "]: " . curl_error($ch), E_USER_ERROR);
        }
        curl_close($ch);

        return $rtn;
    }
}